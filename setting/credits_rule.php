<?php 
 
				
 $config['credits_rule'] = array (
  'reply' => 
  array (
    'rid' => '2',
    'rulename' => '评论或转发微博',
    'action' => 'reply',
    'cycletype' => '4',
    'extcredits1' => '1',
    'extcredits2' => '1',
  ),
  'register' => 
  array (
    'rid' => '4',
    'rulename' => '邀请注册',
    'action' => 'register',
    'cycletype' => '4',
    'extcredits1' => '50',
    'extcredits2' => '50',
  ),
  'down_my_attach' => 
  array (
    'rid' => '22',
    'rulename' => '附件被下载',
    'action' => 'down_my_attach',
    'cycletype' => '4',
    'extcredits2' => '1',
  ),
  'topic_dig' => 
  array (
    'rid' => '23',
    'rulename' => '赞微博',
    'action' => 'topic_dig',
    'cycletype' => '4',
    'extcredits1' => '1',
    'extcredits2' => '1',
  ),
  'convert' => 
  array (
    'rid' => '26',
    'rulename' => '购买虚拟礼物',
    'action' => 'convert',
    'cycletype' => '4',
    'extcredits1' => '1',
  ),
  'unconvert' => 
  array (
    'rid' => '27',
    'rulename' => '退款实体商品',
    'action' => 'unconvert',
    'cycletype' => '4',
    'extcredits1' => '1',
  ),
  '_S' => 
  array (
    'rid' => '28',
    'rulename' => '签到',
    'action' => '_S',
    'cycletype' => '1',
    'extcredits1' => '1',
    'extcredits2' => '1',
  ),
  'virtual_gifts' => 
  array (
    'rid' => '29',
    'rulename' => '送出虚拟礼物',
    'action' => 'virtual_gifts',
    'cycletype' => '4',
    'extcredits1' => '1',
    'extcredits2' => '1',
  ),
  'selection' => 
  array (
    'rid' => '30',
    'rulename' => '评选（试听/试看）',
    'action' => 'selection',
    'cycletype' => '4',
    'extcredits1' => '1',
    'extcredits2' => '1',
  ),
  'recharge' => 
  array (
    'rid' => '31',
    'rulename' => '充值',
    'action' => 'recharge',
    'cycletype' => '4',
    'extcredits1' => '5',
    'extcredits2' => '5',
  ),
  'grab_red_envelope' => 
  array (
    'rid' => '32',
    'rulename' => '红包',
    'action' => 'grab_red_envelope',
    'cycletype' => '4',
    'extcredits1' => '10',
    'extcredits2' => '10',
  ),
  'play_game' => 
  array (
    'rid' => '33',
    'rulename' => '玩游戏',
    'action' => 'play_game',
    'cycletype' => '4',
    'extcredits1' => '10',
    'extcredits2' => '1',
  ),
  'buy' => 
  array (
    'rid' => '34',
    'rulename' => '购买实体商品',
    'action' => 'buy',
    'cycletype' => '4',
    'extcredits1' => '300',
    'extcredits2' => '300',
  ),
  'QA' => 
  array (
    'rid' => '35',
    'rulename' => '问答',
    'action' => 'QA',
    'cycletype' => '4',
    'extcredits1' => '1',
    'extcredits2' => '1',
  ),
); 
?>