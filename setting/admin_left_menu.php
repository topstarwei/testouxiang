<?php

/**
 * [JishiGou] (C)2005 - 2099 Cenwor Inc.
 *
 * This is NOT a freeware, use is subject to license terms
 *
 * @Filename admin_left_menu.php $
 *
 * @Author http://www.jishigou.net $
 *
 * @Date 2013-11-19 37961372 18772 $
 */
$menu_list = array(
    1 =>
    array(
        'title' => '常用操作',
        'link' => 'admin.php?mod=index&code=home',
    ),
    2 =>
    array(
        'title' => '全局',
        'link' => 'admin.php?mod=index&code=home',
        'sub_menu_list' =>
        array(
            /*       0 =>
              array(
              'title' => '<font color="#266AAE">核心设置</font>',
              'link' => 'admin.php?mod=setting&code=modify_normal',
              'shortcut' => true,
              ),
              1 =>
              array(
              'title' => '注册登陆',
              'link' => 'admin.php?mod=setting&code=modify_register',
              'shortcut' => true,
              ), */
            3 =>
            array(
                'title' => '积分规则',
                'link' => 'admin.php?mod=setting&code=list_credits_rule',
                'shortcut' => false,
            ),
            4 =>
            array(
                'title' => '积分等级',
                'link' => 'admin.php?mod=role&code=list&type=normal',
                'shortcut' => false,
            ),
        /* 5 =>
          array(
          'title' => '手机应用',
          'link' => 'admin.php?mod=setting&code=modify_mobile',
          'shortcut' => false,
          ),
          6 =>
          array(
          'title' => '邮件发送',
          'link' => 'admin.php?mod=setting&code=modify_smtp',
          'shortcut' => false,
          ),
          7 =>
          array(
          'title' => '图片附件',
          'link' => 'admin.php?mod=setting&code=modify_image',
          'shortcut' => false,
          ),
          9 =>
          array(
          'title' => '帐户绑定',
          'link' => 'admin.php?mod=setting&code=modify_sina',
          'shortcut' => false,
          ),
          10 =>
          array(
          'title' => '省市区域',
          'link' => 'admin.php?mod=city',
          'shortcut' => false,
          ),
          12 =>
          array(
          'title' => '防灌水验证码',
          'link' => 'admin.php?mod=setting&code=modify_seccode',
          'shortcut' => false,
          ),
          15 =>
          array(
          'title' => '系统整合',
          'link' => 'hr',
          'shortcut' => false,
          ),
          13 =>
          array(
          'title' => '微博评论模块',
          'link' => 'admin.php?mod=output&code=output_setting',
          'shortcut' => true,
          ),
          14 =>
          array(
          'title' => '微博站外调用',
          'link' => 'admin.php?mod=share&code=share_setting',
          'shortcut' => false,
          ), */
        ),
    ),
    3 =>
    array(
        'title' => '界面',
        'link' => 'admin.php?mod=index&code=home',
        'sub_menu_list' =>
        array(
//            0 =>
//            array(
//                'title' => '页面显示',
//                'link' => 'admin.php?mod=show&code=modify',
//                'shortcut' => true,
//            ),
            7 =>
            array(
                'title' => '网站logo',
                'link' => 'admin.php?mod=show&code=editlogo',
                'shortcut' => false,
            ),
        ),
    ),
    4 =>
    array(
        'title' => '内容',
        'link' => '',
        'sub_menu_list' =>
        array(
            0 =>
            array(
                'title' => '微博管理',
                'link' => 'admin.php?mod=topic&code=topic_manage',
                'shortcut' => false,
            ),
            16 =>
            array(
                'title' => '添加微博',
                'link' => 'admin.php?mod=topic&code=topic_add',
                'shortcut' => false,
            ),
            1 =>
            array(
                'title' => '<font color="#266AAE">待审核微博</font>',
                'link' => 'admin.php?mod=topic&code=verify',
                'shortcut' => true,
            ),
            5 =>
            array(
                'title' => '推荐微博',
                'link' => 'admin.php?mod=recdtopic',
                'shortcut' => false,
            ),
            4 =>
            array(
                'title' => '微博举报',
                'link' => 'admin.php?mod=report&code=report_manage',
                'shortcut' => true,
            ),
            3 =>
            array(
                'title' => '内容过滤',
                'link' => 'admin.php?mod=setting&code=modify_filter',
                'shortcut' => true,
            ),
            2 =>
            array(
                'title' => '微博回收站',
                'link' => 'admin.php?mod=topic&code=del&del=1',
                'shortcut' => false,
            ),
//            6 =>
//            array(
//                'title' => '话题和专题',
//                'link' => 'admin.php?mod=tag',
//                'shortcut' => false,
//            ),
//            28 =>
//            array(
//                'title' => '微博属性管理',
//                'link' => 'admin.php?mod=feature',
//                'shortcut' => false,
//            ),
            7 =>
            array(
                'title' => '微博管理记录',
                'link' => 'admin.php?mod=topic&code=manage',
                'shortcut' => false,
            ),
            17 =>
            array(
                'title' => '添加活动',
                'link' => 'admin.php?mod=topic&code=topic_add_activity',
                'shortcut' => false,
            ),
            18 =>
            array(
                'title' => '添加文章',
                'link' => 'admin.php?mod=topic&code=topic_add_article',
                'shortcut' => false,
            ),
//            15 =>
//            array(
//                'title' => 'URL链接管理',
//                'link' => 'admin.php?mod=url&code=manage',
//                'shortcut' => false,
//            ),
            8 =>
            array(
                'title' => '个人信息管理',
                'link' => 'hr',
                'shortcut' => false,
            ),
            13 =>
            array(
                'title' => '私信管理',
                'link' => 'admin.php?mod=pm&code=pm_manage',
                'shortcut' => false,
            ),
            9 =>
            array(
                'title' => '签名管理',
                'link' => 'admin.php?mod=topic&code=signature',
                'shortcut' => false,
            ),
            11 =>
            array(
                'title' => '自我介绍',
                'link' => 'admin.php?mod=topic&code=aboutme',
                'shortcut' => false,
            ),
            12 =>
            array(
                'title' => '个人标签',
                'link' => 'admin.php?mod=user_tag&code=user_tag_manage',
                'shortcut' => false,
            ),
            10 =>
            array(
                'title' => '头像签名审核',
                'link' => 'admin.php?mod=verify&code=fs_verify',
                'shortcut' => false,
            ),
        ),
    ),
    5 =>
    array(
        'title' => '用户',
        'link' => '',
        'sub_menu_list' =>
        array(
            0 =>
            array(
                'title' => '用户列表',
                'link' => 'admin.php?mod=member&code=newm',
                'shortcut' => true,
            ),
            1 =>
            array(
                'title' => '用户绑定情况',
                'link' => 'admin.php?mod=account&code=index',
                'shortcut' => false,
            ),
            2 =>
            array(
                'title' => '待验证用户',
                'link' => 'admin.php?mod=member&code=waitvalidate',
                'shortcut' => false,
            ),
            3 =>
            array(
                'title' => '编辑用户',
                'link' => 'admin.php?mod=member&code=search',
                'shortcut' => true,
            ),
            4 =>
            array(
                'title' => '添加新用户',
                'link' => 'admin.php?mod=member&code=add',
                'shortcut' => false,
            ),
            5 =>
            array(
                'title' => '修改我的资料',
                'link' => 'admin.php?mod=member&code=modify',
                'shortcut' => false,
            ),
//      6 => 
//      array(
//        'title' => '资料自定义',
//        'link' => 'admin.php?mod=member&code=profile',
//        'shortcut' => false,
//      	),
            10 =>
            array(
                'title' => '用户访问记录',
                'link' => 'admin.php?mod=member&code=login',
                'shortcut' => false,
            ),
            11 =>
            array(
                'title' => '导出用户到Excel',
                'link' => 'admin.php?mod=member&code=export_all_user',
                'shortcut' => false,
            ),
            12 =>
            array(
                'title' => '角色权限设置',
                'link' => 'hr',
                'shortcut' => false,
            ),
            13 =>
            array(
                'title' => '管理员角色',
                'link' => 'admin.php?mod=role&code=list&type=admin',
                'shortcut' => false,
            ),
            14 =>
            array(
                'title' => '普通用户角色',
                'link' => 'admin.php?mod=role&code=list&type=normal',
                'shortcut' => false,
            ),
            15 =>
            array(
                'title' => '添加用户角色',
                'link' => 'admin.php?mod=role&code=add',
                'shortcut' => false,
            ),
        ),
    ),
    6 =>
    array(
        'title' => '运营',
        'link' => '',
        'sub_menu_list' =>
        array(
            0 => 
            array (
              'title' => '关于我们',
              'link' => 'admin.php?mod=web_info&code=web_info_setting',
              'shortcut' => false,
            ),
            2 =>
            array(
                'title' => '首页幻灯',
                'link' => 'admin.php?mod=setting&code=modify_slide_index',
                'shortcut' => false,
            ),
//            4 =>
//            array(
//                'title' => '短信群发',
//                'link' => 'admin.php?mod=sms&code=list',
//                'shortcut' => false,
//            ),
        ),
    ),
    7 =>
    array(
        'title' => '应用',
        'link' => '',
        'sub_menu_list' =>
        array(
            /* 1=>
              array (
              'title' => '<font color="#266AAE">频道</font>',
              'link' => 'admin.php?mod=channel&code=index',
              'shortcut' => true,
              ),
              2 =>
              array (
              'title' => '微群',
              'link' => 'admin.php?mod=qun',
              'shortcut' => false,
              ),
              3 =>
              array (
              'title' => '投票',
              'link' => 'admin.php?mod=vote&code=index',
              'shortcut' => false,
              ),
              4 =>
              array (
              'title' => '活动',
              'link' => 'admin.php?mod=event&code=manage',
              'shortcut' => false,
              ), */
//              5 =>
//              array (
//              'title' => '签到',
//              'link' => 'admin.php?mod=sign&code=sign_list',
//              'shortcut' => false,
//              ),
            /* 6 =>
              array (
              'title' => '微直播',
              'link' => 'admin.php?mod=live&code=index',
              'shortcut' => false,
              ),
              7 =>
              array (
              'title' => '微访谈',
              'link' => 'admin.php?mod=talk&code=index',
              'shortcut' => false,
              ),
              8 => array(
              'title' => '有奖转发',
              'link' => 'admin.php?mod=reward',
              'shortcut' => false,
              ),
              25 =>
              array (
              'title' => '微信公共平台',
              'link' => 'admin.php?mod=wechat',
              'shortcut' => false,
              'type' => '1',
              ), */
            27 =>
            array(
                'title' => '商城',
                'link' => 'admin.php?mod=mall&code=goods_list',
                'shortcut' => false,
                'type' => '1',
            ),
            29 =>
            array(
                'title' => '摩币充值',
                'link' => 'admin.php?mod=recharge&code=list',
                'shortcut' => false,
                'type' => '1',
            ),
            28 =>
            array(
                'title' => '抢红包',
                'link' => 'admin.php?mod=activity&code=grab_red_envelope',
                'shortcut' => false,
                'type' => '1',
            ),
            30 =>
            array(
                'title' => '问答',
                'link' => 'admin.php?mod=interlocution&code=lists',
                'shortcut' => false,
                'type' => '1',
            ),
            31 =>
            array(
                'title' => '试听',
                'link' => 'admin.php?mod=audition&code=lists',
                'shortcut' => false,
                'type' => '1',
            ),
            32 =>
            array(
                'title' => '试看',
                'link' => 'admin.php?mod=look&code=lists',
                'shortcut' => false,
                'type' => '1',
            ),
        /* 26 =>
          array (
          'title' => '资讯管理',
          'link' => 'admin.php?mod=cms',
          'shortcut' => false,
          'type' => '1',
          ),
          9 =>
          array(
          'title' => '马甲管理',
          'link' => 'admin.php?mod=member&code=vest',
          'shortcut' => true,
          ),

          11 =>
          array (
          'title' => '<font color="#266AAE">单位和部门</font>',
          'link' => 'admin.php?mod=company',
          'shortcut' => false,
          ),
         * 
          12 =>
          array(
          'title' => 'API应用授权',
          'link' => 'admin.php?mod=api',
          'shortcut' => false,
          ),
          13 =>
          array (
          'title' => '微博秀',
          'link' => 'admin.php?mod=tools&code=weibo_show',
          'shortcut' => false,
          ),
          19 =>
          array (
          'title' => '动态提醒',
          'link' => 'admin.php?mod=feed',
          'shortcut' => false,
          ), */
        ),
    ),
    8 =>
    array(
        'title' => '工具',
        'link' => '',
        'sub_menu_list' =>
        array(
            1 =>
            array(
                'title' => '清空缓存',
                'link' => 'admin.php?mod=cache',
                'shortcut' => false,
            ),
            3 =>
            array(
                'title' => '<font color="#D94446">后台操作记录</font>',
                'link' => 'admin.php?mod=logs',
                'shortcut' => true,
            ),
            4 =>
            array(
                'title' => '数据库管理',
                'link' => 'hr',
                'shortcut' => false,
            ),
            5 =>
            array(
                'title' => '数据备份',
                'link' => 'admin.php?mod=db&code=export',
                'shortcut' => false,
            ),
            6 =>
            array(
                'title' => '数据恢复',
                'link' => 'admin.php?mod=db&code=import',
                'shortcut' => false,
            ),
            7 =>
            array(
                'title' => '数据表优化',
                'link' => 'admin.php?mod=db&code=optimize',
                'shortcut' => false,
            ),
        ),
    ),
);
?>