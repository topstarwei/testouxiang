<?php 
 
				
 $config['groupprofile'] = array (
  'base' => 
  array (
    'key' => 'base',
    'title' => '基本资料',
    'list' => 
    array (
      'mobile' => 
      array (
        'fieldid' => 'mobile',
        'title' => '手机',
        'displayorder' => '7',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'linkaddress' => 
      array (
        'fieldid' => 'linkaddress',
        'title' => '联系地址',
        'displayorder' => '6',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'gender' => 
      array (
        'fieldid' => 'gender',
        'title' => '性别',
        'displayorder' => '3',
        'formtype' => 'select',
        'size' => '0',
        'choices' => '1|男
2|女',
      ),
      'bday' => 
      array (
        'fieldid' => 'bday',
        'title' => '出生年月日',
        'displayorder' => '2',
        'formtype' => 'date',
        'size' => '0',
        'choices' => '',
      ),
      'constellation' => 
      array (
        'fieldid' => 'constellation',
        'title' => '星座',
        'displayorder' => '1',
        'formtype' => 'select',
        'size' => '0',
        'choices' => '白羊座
金牛座
双子座
巨蟹座
狮子座
处女座
天秤座
天蝎座
射手座
摩羯座
水瓶座
双鱼座',
      ),
      'zodiac' => 
      array (
        'fieldid' => 'zodiac',
        'title' => '生肖',
        'displayorder' => '0',
        'formtype' => 'select',
        'size' => '0',
        'choices' => '鼠
牛
虎
兔
龙
蛇
马
羊
猴
鸡
狗
猪',
      ),
      'nationality' => 
      array (
        'fieldid' => 'nationality',
        'title' => '国籍',
        'displayorder' => '0',
        'formtype' => 'select',
        'size' => '0',
        'choices' => '阿尔巴尼亚
阿尔及利亚
阿富汗
阿根廷
阿拉伯联合酋长国
阿鲁巴
阿曼
阿塞拜疆
埃及
埃塞俄比亚
爱尔兰
爱沙尼亚
安道尔
安哥拉
安圭拉岛
安提瓜和巴布达
奥地利
奥兰岛
澳大利亚
澳门特别行政区
巴巴多斯
巴布亚新几内亚
巴哈马
巴基斯坦
巴拉圭
巴勒斯坦民族权力机构
巴林
巴拿马
巴西
白俄罗斯
百慕大群岛
保加利亚
北马里亚纳群岛
贝宁
比利时
冰岛
波多黎各
波兰
波斯尼亚和黑塞哥维那
玻利维亚
伯利兹
博茨瓦纳
博内尔
不丹
布基纳法索
布隆迪
布韦岛
朝鲜
赤道几内亚
丹麦
德国
东帝汶
多哥
多米尼加共和国
多米尼克
俄罗斯
厄瓜多尔
厄立特里亚
法国
法罗群岛
法属波利尼西亚
法属圭亚那
法属南极地区
梵蒂冈城
菲律宾
斐济群岛
芬兰
佛得角
福克兰群岛
冈比亚
刚果(DRC)
刚果共和国
哥伦比亚
哥斯达黎加
格恩西岛
格林纳达
格陵兰
格鲁吉亚
古巴
瓜德罗普岛
关岛
圭亚那
哈萨克斯坦
海地
韩国
荷兰
赫德和麦克唐纳群岛
黑山共和国
洪都拉斯
基里巴斯
吉布提
吉尔吉斯斯坦
几内亚
几内亚比绍
加拿大
加纳
加蓬
柬埔寨
捷克共和国
津巴布韦
喀麦隆
卡塔尔
开曼群岛
科科斯群岛(基灵群岛)
科摩罗联盟
科特迪瓦共和国
科威特
克罗地亚
肯尼亚
库可群岛
库拉索
拉脱维亚
莱索托
老挝
黎巴嫩
立陶宛
利比里亚
利比亚
列支敦士登
留尼汪岛
卢森堡
卢旺达
罗马尼亚
马达加斯加
马恩岛
马尔代夫
马耳他
马拉维
马来西亚
马里
马其顿
马绍尔群岛
马提尼克岛
马约特岛
毛里求斯
毛利塔尼亚
美国
美属萨摩亚
美属外岛
美属维尔京群岛
蒙古
蒙特塞拉特
孟加拉国
秘鲁
密克罗尼西亚
缅甸
摩尔多瓦
摩洛哥
摩纳哥
莫桑比克
墨西哥
纳米比亚
南非
南极洲
南乔治亚和南德桑威奇群岛
瑙鲁
尼泊尔
尼加拉瓜
尼日尔
尼日利亚
纽埃
挪威
诺福克岛
帕劳群岛
皮特凯恩群岛
葡萄牙
日本
瑞典
瑞士
萨尔瓦多
萨摩亚
塞尔维亚共和国
塞拉利昂
塞内加尔
塞浦路斯
塞舌尔
沙巴岛
沙特阿拉伯
圣巴泰勒米岛
圣诞岛
圣多美和普林西比
圣赫勒拿岛
圣基茨和尼维斯
圣卢西亚
法属圣马丁岛
荷属圣马丁岛
圣马力诺
圣皮埃尔岛和密克隆岛
圣文森特和格林纳丁斯
圣尤斯特歇斯岛
斯里兰卡
斯洛伐克
斯洛文尼亚
斯威士兰
苏丹
苏里南
所罗门群岛
索马里
塔吉克斯坦
台湾
泰国
坦桑尼亚
汤加
特克斯和凯科斯群岛
特立尼达和多巴哥
突尼斯
图瓦卢
土耳其
土库曼斯坦
托克劳
瓦利斯和富图纳
瓦努阿图
危地马拉
维尔京群岛(英属)
委内瑞拉
文莱
乌干达
乌克兰
乌拉圭
乌兹别克斯坦
西班牙
希腊
香港特别行政区
新加坡
新喀里多尼亚
新西兰
匈牙利
叙利亚
牙买加
亚美尼亚
扬马延岛
也门
伊拉克
伊朗
以色列
意大利
印度
印度尼西亚
英国
英属印度洋领地
约旦
越南
赞比亚
泽西
乍得
直布罗陀
智利
中非共和国
中国
其他',
      ),
      'birthcity' => 
      array (
        'fieldid' => 'birthcity',
        'title' => '出生地',
        'displayorder' => '0',
        'formtype' => 'select',
        'size' => '0',
        'choices' => '',
      ),
      'bloodtype' => 
      array (
        'fieldid' => 'bloodtype',
        'title' => '血型',
        'displayorder' => '0',
        'formtype' => 'select',
        'size' => '0',
        'choices' => 'A
B
AB
O
其他',
      ),
      'height' => 
      array (
        'fieldid' => 'height',
        'title' => '身高',
        'displayorder' => '0',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'weight' => 
      array (
        'fieldid' => 'weight',
        'title' => '体重',
        'displayorder' => '0',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'aboutme' => 
      array (
        'fieldid' => 'aboutme',
        'title' => '个人介绍',
        'displayorder' => '0',
        'formtype' => 'textarea',
        'size' => '0',
        'choices' => '',
      ),
      'Chinese_name' => 
      array (
        'fieldid' => 'Chinese_name',
        'title' => '中文名',
        'displayorder' => '0',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'foreign_name' => 
      array (
        'fieldid' => 'foreign_name',
        'title' => '外文名',
        'displayorder' => '0',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'alias' => 
      array (
        'fieldid' => 'alias',
        'title' => '别名',
        'displayorder' => '0',
        'formtype' => 'text',
        'size' => '0',
        'choices' => '',
      ),
      'nation' => 
      array (
        'fieldid' => 'nation',
        'title' => '民族',
        'displayorder' => '0',
        'formtype' => 'select',
        'size' => '0',
        'choices' => '汉族
壮族
满族
回族
苗族
维吾尔族
土家族
彝族
蒙古族
藏族
布依族
侗族
瑶族
朝鲜族
白族
哈尼族
哈萨克族
黎族
傣族
畲族
傈僳族
仡佬族
东乡族
高山族
拉祜族
水族
佤族
纳西族
羌族
土族
仫佬族
锡伯族
柯尔克孜族
达斡尔族
景颇族
毛南族
撒拉族
布朗族
塔吉克族
阿昌族
普米族
鄂温克族
怒族
京族
基诺族
德昂族
保安族
俄罗斯族
裕固族
乌孜别克族
门巴族
鄂伦春族
独龙族
塔塔尔族
赫哲族
珞巴族
其他',
      ),
    ),
  ),
); 
?>