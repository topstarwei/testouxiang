<?php

/**
 *
 * 数据表 grab_red_envelope 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_grab_red_envelope_log extends table {

    var $table = 'grab_red_envelope_log';

    function table_grab_red_envelope_log() {
        $this->init($this->table);
    }

}

?>