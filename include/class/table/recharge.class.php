<?php

/**
 *
 * 数据表 recharge 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_recharge extends table {

    var $table = 'recharge';

    function table_recharge() {
        $this->init($this->table);
    }

}

?>