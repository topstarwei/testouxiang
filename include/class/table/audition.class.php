<?php

/**
 *
 * 数据表 audition 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_audition extends table {

    var $table = 'audition';

    function table_audition() {
        $this->init($this->table);
    }

}

?>