<?php

/**
 *
 * 数据表 tcosler 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_tcosler extends table {

    var $table = 'tcosler';

    function table_tcosler() {
        $this->init($this->table);
    }

}

?>