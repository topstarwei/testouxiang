<?php
/**
 *
 * 数据表 mall_cart 相关操作类
 *
 *
 * This is NOT a freeware, use is subject to license terms
 *
 * @copyright Copyright (C) 2005 - 2099 Cenwor Inc.
 * @license http://www.cenwor.com
 * @link http://www.jishigou.net
 * @author 帅帅<foxis@qq.com>
 * @version $Id: mall_cart.class.php 701455476 2016-05-11
 */

if(!defined('IN_JISHIGOU')) {
	exit('invalid request');
}

class table_mall_cart extends table {
	
	
	var $table = 'mall_cart';
	
	function table_mall_cart() {
		$this->init($this->table);
	}
		
}

?>