<?php

/**
 *
 * 数据表 audition_log 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_audition_log extends table {

    var $table = 'audition_log';

    function table_audition_log() {
        $this->init($this->table);
    }

}

?>