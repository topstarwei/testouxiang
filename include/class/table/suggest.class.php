<?php

/**
 *
 * 数据表 suggest 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_suggest extends table {

    var $table = 'suggest';

    function table_suggest() {
        $this->init($this->table);
    }

}

?>