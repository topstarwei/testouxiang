<?php
/**
 *
 * 数据表 emay_sms 相关操作类
 *
 *
 * This is NOT a freeware, use is subject to license terms
 *
 * @copyright Copyright (C) 2005 - 2099 Cenwor Inc.
 * @license http://www.cenwor.com
 * @link http://www.jishigou.net
 * @author 刘<foxis@qq.com>
 * @version $Id: emay_sms.class.php 3678 2016-04-12 12:37:20Z liuaiyong $
 */

if(!defined('IN_JISHIGOU')) {
	exit('invalid request');
}

class table_emay_sms extends table {
	
	
	var $table = 'emay_sms';
	
	function table_emay_sms() {
		$this->init($this->table);
	}
	function row($where, $return = '') {
            $row = $this->get($where,$return);
            return $row;
	}	
}

?>