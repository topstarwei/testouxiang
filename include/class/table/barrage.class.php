<?php

/**
 *
 * 数据表 barrage 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_barrage extends table {

    var $table = 'barrage';

    function table_barrage() {
        $this->init($this->table);
    }

}

?>