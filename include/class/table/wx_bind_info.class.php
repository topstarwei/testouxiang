<?php

/**
 *
 * 数据表 wx_bind_info 相关操作类
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_wx_bind_info extends table {

    var $table = 'wx_bind_info';

    function table_wx_bind_info() {
        $this->init($this->table);
    }

}

?>