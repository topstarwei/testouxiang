<?php

/**
 *
 * 数据表 look_log 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_look_log extends table {

    var $table = 'look_log';

    function table_look_log() {
        $this->init($this->table);
    }

}

?>