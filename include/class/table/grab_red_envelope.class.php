<?php

/**
 *
 * 数据表 grab_red_envelope 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_grab_red_envelope extends table {

    var $table = 'grab_red_envelope';

    function table_grab_red_envelope() {
        $this->init($this->table);
    }

}

?>