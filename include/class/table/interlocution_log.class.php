<?php

/**
 *
 * 数据表 interlocution_log 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_interlocution_log extends table {

    var $table = 'interlocution_log';

    function table_interlocution_log() {
        $this->init($this->table);
    }

}

?>