<?php

/**
 *
 * 数据表 mall_collect 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_mall_collect extends table {

    var $table = 'mall_collect';

    function table_mall_collect() {
        $this->init($this->table);
    }

}

?>