<?php

/**
 *
 * 数据表 recharge_log 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_recharge_log extends table {

    var $table = 'recharge_log';

    function table_recharge_log() {
        $this->init($this->table);
    }

}

?>