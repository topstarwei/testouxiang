<?php

/**
 *
 * 数据表 interlocution 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_interlocution extends table {

    var $table = 'interlocution';

    function table_interlocution() {
        $this->init($this->table);
    }

}

?>