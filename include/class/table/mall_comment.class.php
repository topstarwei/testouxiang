<?php

/**
 *
 * 数据表 mall_comment 相关操作类
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class table_mall_comment extends table {

    var $table = 'mall_comment';

    function table_mall_comment() {
        $this->init($this->table);
    }

}

?>