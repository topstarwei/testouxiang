<?php

if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}
set_time_limit(0);
header("Content-Type: text/html; charset=UTF-8");

class erp {

    /**
     * 接口请求地址
     */
    var $gwurl = 'http://316050913407329.try.taoex.com/index.php/openapi/rpc/service/';

    /**
     * 访问来源标识
     */
    var $flag = 'somall';

    /**
     * 签名
     */
    var $sign = 'AplJKmDMJoOcLDmgyEJOxGJHbUpJokzT';

    var $type = 'json';
    
    var $charset = 'utf-8';
    
    var $ver = 1;
    /**
     * 格式化参数格式化成url参数
     */
    public function ToUrlParams($values = array()) {
        $buff = "";
        foreach ($values as $k => $v) {
            if ($k != "sign" && $v != null && !is_array($v)) {
                $buff .= $k . $v;
            }
        }
        $buff = md5($buff);
        $buff = strtoupper($buff);
        return $buff;
    }

    /**
     * 生成签名
     * @return 签名
     */
    public function MakeSign($values = array()) {
        //签名步骤一：按字典序排序参数
        
        ksort($values);
        $string = $this->ToUrlParams($values);
        //签名步骤二：在string后加入KEY
        $string = $string . $this->sign;
        //签名步骤三：MD5加密
        $string = md5($string);
        //签名步骤四：所有字符转为大写
        $result = strtoupper($string);
        return $result;
    }
    public function getcontent($arr = array()){
        $arr['flag'] = $this->flag;
        $arr['type'] = $this->type;
        $arr['charset'] = $this->charset;
        $arr['ver'] = $this->ver;
        $arr['timestamp'] = time();
        $new_arr = array_filter($arr);
        $sign = $this->MakeSign($new_arr);
        $new_arr['sign'] = $sign;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $this->gwurl);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $new_arr);
        $output = curl_exec($curl);
        curl_close($curl);
        $output = json_decode($output, True);

        return $output;
    }
    
}

?>
