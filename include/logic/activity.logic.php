<?php

/**
 *
 * activity的相关操作
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ActivityLogic {

    function __construct() {
        ;
    }

    public function get_red_envelope_list($sql_where = '', $order = "id desc", $limit = 10) {
        return jtable("grab_red_envelope")->get(array("sql_where" => $sql_where, "sql_order" => $order, "page_num" => $limit));
    }
    public function get_red_envelopelog_list($sql_where = '', $order = "id desc", $limit = 10) {
        return jtable("grab_red_envelope_log")->get(array("sql_where" => $sql_where, "sql_order" => $order, "page_num" => $limit));
    }
    public function get_red_envelopelog_tatal($where,$group_by=''){
        $sql = "select sum(undraw_amount) as undraw_amount FROM `" . DB::table('grab_red_envelope') . "` where 1 {$where} {$group_by}";
        return DB::fetch_first($sql);
    }
}
