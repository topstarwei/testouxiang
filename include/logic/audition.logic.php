<?php

/**
 *
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class AuditionLogic {

    function __construct() {
        ;
    }

    public function get_list($sql_where = '', $order = "id desc", $limit = 10) {
        return jtable("audition")->get(array("sql_where" => $sql_where, "sql_order" => $order, "page_num" => $limit));
    }
    public function get_log_list($sql_where = '', $order = "id desc", $limit = 10) {
        return jtable("audition_log")->get(array("sql_where" => $sql_where, "sql_order" => $order, "page_num" => $limit));
    }
    public function get_list_bypage($where, $page=1, $page_size=20,$order="id desc"){
        $offset = ($page-1)*$page_size;
        $sql = "select * FROM `" . DB::table('audition') . "` where 1 {$where}  order BY {$order} LIMIT {$offset} , {$page_size}";
        return DB::fetch_all($sql);
    }
}
