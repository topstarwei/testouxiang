<?php

/**
 *
 * barrage的相关操作
 *
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class BarrageLogic {

    function __construct() {
        ;
    }

    public function get_list_bypage($where, $page=1, $page_size=20,$order="id desc"){
        $offset = ($page-1)*$page_size;
        $sql = "select b.*,m.nickname,m.face FROM `" . DB::table('barrage') . "` b left join `" . DB::table('members') . "` m on b.uid = m.uid where 1 {$where}  order BY {$order} LIMIT {$offset} , {$page_size}";
        return DB::fetch_all($sql);
    }
}
