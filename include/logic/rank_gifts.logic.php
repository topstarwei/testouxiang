<?php

/**
 *
 * rank_gifts的相关操作
 * 
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class RankGiftsLogic {

    function __construct() {
        ;
    }

    public function get_rankgiftslist_bygroup($where, $page=1, $page_size=20){
        $offset = ($page-1)*$page_size;
        $sql = "select g.*,m.face,m.nickname,sum(num) as rank_num FROM `" . DB::table('ranking_gifts') . "` g left join `" . DB::table('members') . "` m on from_uid = uid where 1 {$where}  GROUP BY from_uid ORDER BY rank_num desc LIMIT {$offset} , {$page_size}";
        return DB::fetch_all($sql);
    }
    public function get_rankgiftslist($where){
        $sql = "select *,sum(num) as rank_num FROM `" . DB::table('ranking_gifts') . "` where 1 {$where}  GROUP BY from_uid ORDER BY rank_num desc";
        return DB::fetch_all($sql);
    }
    public function get_rankgifts_bygroup($where,$group_by=''){
        $sql = "select *,sum(num) as rank_num FROM `" . DB::table('ranking_gifts') . "` where 1 {$where} {$group_by}";
        return DB::fetch_first($sql);
    }
    
    public function get_mysendgifts($where, $page=1, $page_size=20){
        $offset = ($page-1)*$page_size;
        $sql = "select m.*,g.num,g.to_uid,g.date,g.datetime,g.from_nickname,g.to_nickname FROM `" . DB::table('ranking_gifts') . "` g left join `" . DB::table('mall_goods') . "` m on g.gid = m.id where 1 {$where}  GROUP BY g.id desc LIMIT {$offset} , {$page_size}";
        return DB::fetch_all($sql);
    }
}
