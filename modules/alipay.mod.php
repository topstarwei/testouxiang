<?php

/**
 * 功能描述：支付回调
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }

    function Execute() {

        switch ($this->Code) {
            case 'index':
                $this->Alipay();
                break;
            default:
                $this->Alipay();
                break;
        }
    }

    public function Alipay() {
        unset($_POST["mod"]);
        unset($_POST["code"]);
        unset($_GET["mod"]);
        unset($_GET["code"]);
        define('ALIPAY_ROOT', $_SERVER["DOCUMENT_ROOT"] . '/alipay/');
        require_once ALIPAY_ROOT . 'alipay.config.php';
        require_once ALIPAY_ROOT . 'lib/alipay_notify.class.php';
        //计算得出通知验证结果
        $alipayNotify = new AlipayNotify($alipay_config);
        $verify_result = $alipayNotify->verifyNotify();
        if ($verify_result) {//验证成功
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];

            //支付宝交易号

            $trade_no = $_POST['trade_no'];

            //交易状态
            $trade_status = $_POST['trade_status'];


            if ($_POST['trade_status'] == 'TRADE_FINISHED') {
                
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                $p = array(
                    'ordersid' => $out_trade_no,
                    'status' => '0'
                );
                $row = jtable('mall_order')->get($p);
                if ($row['list']){
                    $updata = array(
                        'status' => '1',
                        'pay_time' => time(),
                        'post_str' => $this->createLinkString($_POST)
                    );
                    jtable('mall_order')->update($updata,array('ordersid'=>$out_trade_no));
                    foreach ($row['list'] as $value) {
                        $uid = $value['uid'];
                        $goods_type = $value['goods_style'];
                        if($goods_type == 'fictitious'){
                            
                            $member = jsg_member_info($uid);
                            jtable('members')->update(array('extcredits2'=>($member['extcredits2']-$value['pay_gold'])),array('uid'=>$uid));
                            update_credits_by_action('convert', $uid);
                            update_credits_by_action('consume',$uid);
                            $starttime = strtotime(date('Y-m-d 00:00:00', time()));
                            $endtime = strtotime(date('Y-m-d 23:59:59', time()));
                            $where_arr = array(
                                'uid' => $uid,
                                'goods_style' => 'fictitious',
                                '>=@pay_time' => $starttime,
                                '<=@pay_time' => $endtime,
                                '>=@status' => 1,
                                '<=@status' => 2
                            );
                            $count = jtable('mall_order')->count($where_arr);
                            if($count >= 10){
                                update_credits_by_action('convert-ten',$uid);
                            }
                            $gifts = jtable('my_gifts')->info(array('uid'=>$uid,'gid'=>$value['goods_id']));
                            if(is_array($gifts)){
                                $savedata = array(
                                    'uid'=>$uid,
                                    'gid'=>$value['goods_id'],
                                    'num' => $gifts['num'] + $value['goods_num'],
                                );
                                jtable('my_gifts')->update($savedata,array('uid'=>$uid,'gid'=>$value['goods_id']));
                            }else{
                                $savedata = array(
                                    'uid'=>$uid,
                                    'gid'=>$value['goods_id'],
                                    'num' => $value['goods_num'],
                                );
                                jtable('my_gifts')->insert($savedata);
                            }
                        }else{
                            update_credits_by_action('buy', $uid);
                            update_credits_by_action('consume',$uid);
                            $starttime = strtotime(date('Y-m-d 00:00:00', time()));
                            $endtime = strtotime(date('Y-m-d 23:59:59', time()));
                            $where_arr = array(
                                'uid' => $uid,
                                'goods_style' => 'entity',
                                '>=@pay_time' => $starttime,
                                '<=@pay_time' => $endtime,
                                '>=@status' => 1,
                                '<=@status' => 2
                            );
                            $count = jtable('mall_order')->count($where_arr);
                            if($count >= 5){
                                update_credits_by_action('buy-five',$uid);
                            }
                        }
                    }
                    
                }else{
                    $recharge_p = array(
                        'ordersid' => $out_trade_no,
                        'pay_status' => '0'
                    );
                    $recharge = jtable('recharge_log')->info($recharge_p);
                    if(is_array($recharge)){
                        $updata = array(
                            'pay_status' => 1,
                            'pay_time' => date('Y-m-d H:i:s', time())
                        );
                        jtable('recharge_log')->update($updata,array('ordersid'=>$out_trade_no));
                        $uid = $recharge['r_uid'];
                        $member = jsg_member_info($uid);
                        jtable('members')->update(array('extcredits2'=>($member['extcredits2']+$recharge['get_coin'])),array('uid'=>$uid));
                        update_credits_by_action('recharge', $uid);
                        update_credits_by_action('recharging',$uid);
                        $starttime = date('Y-m-d 00:00:00', time());
                        $endtime = date('Y-m-d 23:59:59', time());
                        $where_arr = array(
                            'r_uid' => $uid,
                            '>=@pay_time' => $starttime,
                            '<=@pay_time' => $endtime,
                            'pay_status' => '1'
                        );
                        $count = jtable('recharge_log')->count($where_arr);
                        if($count >= 5){
                            update_credits_by_action('recharge-five',$uid);
                        }
                    }
                }
            }

            //——请根据您的业务逻辑来编写程序（以上代码仅作参考）——

            //echo "success";  //请不要修改或删除
            /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        } else {
            //验证失败
            //echo "fail";
        }
    }
    function createLinkstring($para) {
        $arg  = "";
        while (list ($key, $val) = each ($para)) {
                $arg.=$key."=".$val."&";
        }
        //去掉最后一个&字符
        $arg = substr($arg,0,count($arg)-2);

        //如果存在转义字符，那么去掉转义
        if(get_magic_quotes_gpc()){$arg = stripslashes($arg);}

        return $arg;
    }

}

?>