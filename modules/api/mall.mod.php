<?php

/**
 * 文件名：mall.mod.php
 * @version $Id: member.mod.php 5501 2014-01-23 02:28:27Z chenxianfeng $
 * 作者：帅帅
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject(& $config) {
        $this->MasterObject($config);


        $this->Execute();
    }

    function Execute() {
        ob_start();
//        jsp_ckeck_signature();
//        $uncheckdata = array('checkpay','checkwx','login','doregister','sendsms','verifycode','sinaauthcallback','qqauthcallback','wxauthcallback');
//        if(!in_array($this->Code,$uncheckdata)){
//            jsp_login_check();
//        }
        switch ($this->Code) {
            case 'goodslist':
                $this->GoodsList();
                break;
            case 'goodsdetail':
                $this->GoodsDetail();
                break;
            case 'addcart':
                $this->AddCart();
                break;
            case 'cartlist':
                $this->CartList();
                break;
            case 'delcart':
                $this->DelCart();
                break;
            case 'modifycart':
                $this->ModifyCart();
                break;
            case 'orderprolist':
                $this->OrderProList();
                break;
            case 'addorder':
                $this->AddOrder();
                break;
            case 'orderlist':
                $this->OrderList();
                break;
            case 'orderdetail':
                $this->OrderDetail();
                break;
            case 'buynow':
                $this->BuyNow();
                break;
            case 'fictitiousorder':
                $this->FictitiousOrder();
                break;
            case 'addfictitiousorder':
                $this->AddFictitiousOrder();
                break;
            case 'docollect':
                $this->DoCollect();
                break;
            case 'collectlist':
                $this->CollectList();
                break;
            case 'delcollect':
                $this->DelCollect();
                break;
            case 'myadress':
                $this->MyAdress();
                break;
            case 'saveadress':
                $this->SaveAdress();
                break;
            case 'rechargelist':
                $this->RechargeList();
                break;
            case 'recharge':
                $this->Recharge();
                break;
            case 'addcomment':
                $this->AddComment();
                break;
            case 'addsuggest':
                $this->AddSuggest();
                break;
            case 'commentlist':
                $this->CommentList();
                break;
            case 'getgoodslist':
                $this->GetGoodsList();
                break;
            case 'checkpay':
                $this->CheckPay();
                break;
            default:
                $this->Main();
                break;
        }
        response_text(ob_get_clean());
    }

    function Main() {
        response_text("正在建设中……");
    }
    public function GetGoodsList(){
        $arr = array(
            'brand_name' => '',
            'type_name' => '',
            'goods_bn' => '',
            'start_lastmodify' => '',
            'end_lastmodify' => '',
            'page_no' => 1,
            'page_size' => 10,
            'method' => 'goods.getList'
        );
        $json = jclass('erp')->getcontent($arr);
        json_result('加载成功', $json);
    }
    /**
     * 产品列表
     */
    function GoodsList() {
        $style = jget('style');
        $sql_where = ' 1 ';
        if(!isset($style) && $style == '' ){
            $style = 'entity';
        }
        $sql_where .= " and style = '$style' ";
        $list  = jlogic('mall')->get_goods_list('all',$sql_where);
        foreach ($list['list'] as $k => $one) {
            $list['list'][$k]['expire'] = my_date_format($one['expire']);
        }  
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['list'] = $list;
        $slide_list = array();
        $slide_config = jconf::get('slide_mall');
        if($slide_config){
            $slide_list = $slide_config['list'];
        }
        $json['slide_list'] = $slide_list;
        json_result('加载成功', $json);
    }
    
    function GoodsDetail() {
        $id = jget("id", 'int');
        if ($id < 1) {
            json_error("商品不存在");
        }
        $info = jlogic("mall")->get_info($id);  
        $comment = jlogic("mall")->get_comment_bygroup(" and gid=".$id," group by gid ");  
        $ids = 0;
        $ratio = 100;
        if($comment){
            $ids = $comment['ids'];
            $score_total = $comment['score_total'];
            $ratio = ($score_total/($ids*5))*100;
        }
        $info['expire'] = my_date_format($info['expire']);
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['info'] = $info;
        $json['ids'] = $ids;
        $json['score_ratio'] = $ratio;
        json_result('加载成功', $json);
    }
    /**
     * 添加购物车
     */
    function AddCart(){
        $uid = jget('uid','int');
        $id = jget('id','int');
        $num = jget('num','int');
        $property_key = jget('property_key');
        $property_val = jget('property_val');
        if ($uid < 1) {
            json_error("请登录");
        }
        if ($id < 1) {
            json_error("商品不存在");
        }
        if ($num < 1) {
            json_error("请选择数量");
        }
        $property_array['key'] = $property_key;
        $property_array['val'] = $property_val;
        $parameter = json_encode($property_array, JSON_UNESCAPED_UNICODE);
        $where = array(
            'uid' => $uid,
            'goods_id' => $id,
            'parameter' => $parameter
        );
        $ret = jlogic("mall")->get_cart_info($where);
        if(is_array($ret)){
            $num = $num + $ret['goods_num'];
            jtable('mall_cart')->delete($ret['id']);
        }
        $savedata = array(
            'uid' => $uid,
            'goods_id' => $id,
            'goods_num' => $num,
            'parameter' => $parameter,
            'ctime' => (int) time()
        );
        jlogic("mall")->add_cart($savedata, 1);
        json_result('加入成功');
    }
    /**
     * 购物车列表
     */
    function CartList() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $where = "uid = ".$uid;
        $list  = jlogic('mall')->get_cart_list($where);
        foreach ($list['list'] as $k => $one) {
            $info = jlogic("mall")->get_info($one['goods_id']);  
            $list['list'][$k]['ctime'] = my_date_format($one['ctime']);
            $list['list'][$k]['info'] = $info;
        }  
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['list'] = $list;
        json_result('加载成功', $json);
    }
    /**
     * 删除购物车
     */
    function DelCart() {
        $id = jget("id");
        jtable('mall_cart')->delete($id);
        json_result('删除成功');
    }
    /**
     * 修改购物车产品数量
     */
    function ModifyCart(){
        $cids = jget('cid');
        $pids = jget('pid');
        $nums = jget('num');
        $strcids = preg_replace('/[\"\[\]]/', '', $cids);
        $strcidarr = explode(',', $strcids);
        $strpids = preg_replace('/[\"\[\]]/', '', $pids);
        $strpidarr = explode(',', $strpids);
        $strnums = preg_replace('/[\"\[\]]/', '', $nums);
        $strnumarr = explode(',', $strnums);
        if(count($strcidarr)>0){
            foreach ($strcidarr as $key => $value) {
                $updata = array(
                    'goods_id' => $strpidarr[$key],
                    'goods_num' => $strnumarr[$key]
                );
                jtable("mall_cart")->update($updata, $value);
            }
        }
        json_result('编辑成功');
    }
    /**
     * 下单页面产品列表
     */
    function OrderProList() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $cids = jget('cid');
        $strcids = preg_replace('/[\"\[\]]/', '', $cids);
        if($strcids != ''){
            $where = "id in (".$strcids.") and uid = ".$uid;
            $list  = jlogic('mall')->get_cart_list($where);
            foreach ($list['list'] as $k => $one) {
                $info = jlogic("mall")->get_info($one['goods_id']);  
                $list['list'][$k]['ctime'] = my_date_format($one['ctime']);
                $list['list'][$k]['info'] = $info;
            }  
            
            $json['site_url'] = $GLOBALS['_J']['site_url'];
            $json['list'] = $list;
            $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
            $json['address'] = $address;
            json_result('加载成功', $json);
        }else{
            json_error("请选择商品");
        }
        
    }
    
    /**
     * 添加订单
     */
    function AddOrder() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $receiver_address = trim(jget('address', 'txt'));
        $receiver_tel = jget('receiver_tel', 'mobile');
        $receiver_name = trim(jget('receiver_name'),'txt');
        $receiver_state = trim(jget('receiver_state'),'txt');
        $receiver_city = trim(jget('receiver_city'),'txt');
        $receiver_district = trim(jget('receiver_district'),'txt');
        $postcode = trim(jget('postcode'),'txt');
        $pay_type = jget('pay_type');
        if($receiver_name == ''){
            return json_error('请填写收货人姓名');
        }
        if($postcode == ''){
            return json_error('请填写邮编');
        }
        if (strlen($receiver_address) <= 0) {
            return json_error('请填写有效的送货地址');
        }
        
        if (empty($receiver_tel)) {
            return json_error('请填写正确的手机号码');
        }
        $cids = jget('cidArr');
        $strcids = preg_replace('/[\"\[\]]/', '', $cids);
        if($strcids == ''){
            return json_error('请选择商品！');
        }
        $where = "id in (".$strcids.") and uid = ".$uid;
        $list  = jlogic('mall')->get_cart_list($where);
        $member = jsg_member_info($uid);
        $ordersid = date('YmdHis', time()) . mt_rand(1000, 9999);
        foreach ($list['list'] as $k => $one) {
            
            $info = jlogic('mall')->get_info($one['goods_id']);
            if (TIMESTAMP > ($info['expire'])) {
                return json_error('商品已过期！');
            }
            $num = $one['goods_num'];
            if ($num > $info['total']) {
                return json_error('库存商品不足！');
            }
            
            $data = array(
                'uid' => $uid,
                'username' => $member['nickname'],
                'ordersid' => $ordersid,
                'goods_id' => $info['id'],
                'goods_name' => $info['name'],
                'goods_num' => $num,
                'goods_price' => $info['price'],
                'goods_style' => $info['style'],
                'pay_price' => $info['price']*$num,
                'receiver_name' => $receiver_name,
                'address' => $receiver_address,
                'receiver_tel' => $receiver_tel,
                'postcode' => $postcode,
                'receiver_state' => $receiver_state,
                'receiver_city' => $receiver_city,
                'receiver_district' => $receiver_district,
                'parameter' => $one['parameter'],
                'pay_type' => $pay_type,
                'add_time' => TIMESTAMP,
                'pay_time' => TIMESTAMP,
                'status' => 0,
            );
            if($pay_type == 'COD'){
                $data['status'] = 1;
            }
            jlogic('mall_order')->add_order($data);
            $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
            if(is_array($address)){
                $address_data = array(
                    'receiver_uid' => $uid,
                    'receiver_name' => $receiver_name,
                    'receiver_tel' => $receiver_tel,
                    'receiver_state' => $receiver_state,
                    'receiver_city' => $receiver_city,
                    'receiver_district' => $receiver_district,
                    'receiver_address' => $receiver_address,
                    'receiver_postcode' => $postcode
                );
                jtable('mall_address')->update($address_data,array('id'=>$address['id']));
            }else{
                $address_data = array(
                    'receiver_uid' => $uid,
                    'receiver_name' => $receiver_name,
                    'receiver_tel' => $receiver_tel,
                    'receiver_state' => $receiver_state,
                    'receiver_city' => $receiver_city,
                    'receiver_district' => $receiver_district,
                    'receiver_address' => $address,
                    'receiver_postcode' => $postcode,
                    'addtime' => date('Y-m-d H:i:s', time())
                );
                jtable('mall_address')->insert($address_data);
            }
            jtable('mall_cart')->delete($one['id']);
        }
        $json['ordersid'] = $ordersid;
        return json_result('商品购买成功',$json);
    }
    /**
     * 订单列表页
     */
    function OrderList() {      
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $where = '';
        $where .= 'and uid = '.$uid;
        $where .= "and goods_style = 'entity' ";
        $page = jget('page','int');
        if(!isset($page)){
            $page = 1;
        }
        $page_size = 20;
        $order = 'add_time desc';
        $orders  = jlogic('mall_order')->get_orderlist_bygroup($where,$page,$page_size,$order);
        $oids = array();
        foreach ($orders as $value) {
            if($value['ordersid'] != ''){
                $oids[] = $value['ordersid'];
            }
        }
        $stroid = implode(',', $oids);
        $json = array();
        $orderlist = array();
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        if($stroid == ''){
            $json['orderlist'] = $orderlist;
            return json_result('加载成功', $json);
        }
        $where = "ordersid in (".$stroid.")";
        $list  = jlogic('mall_order')->get_order_list($where);
        
        foreach ($list['list'] as $k => $one) {
            $info = jlogic("mall")->get_info($one['goods_id']);  
            $list['list'][$k]['add_time'] = my_date_format($one['add_time']);
            $list['list'][$k]['pay_time'] = my_date_format($one['pay_time']);
            $list['list'][$k]['confirm_time'] = my_date_format($one['confirm_time']);
            $list['list'][$k]['info'] = $info;
        }  
        foreach ($list['list'] as $k => $one) {
            $orderlist[$one['ordersid']][] = $one;
        } 
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['orderlist'] = $orderlist;
        json_result('加载成功', $json);
    }
    /**
     * 订单详情页
     */
    function OrderDetail() {
        $oid = jget('oid','int');
        if ($oid < 1) {
            json_error("订单不存在");
        }
        
        $where = "ordersid = ".$oid;
        $list  = jlogic('mall_order')->get_order_list($where);
        foreach ($list['list'] as $k => $one) {
            $info = jlogic("mall")->get_info($one['goods_id']);  
            $list['list'][$k]['add_time'] = my_date_format($one['add_time']);
            $list['list'][$k]['pay_time'] = my_date_format($one['pay_time']);
            $list['list'][$k]['confirm_time'] = my_date_format($one['confirm_time']);
            unset($info['desc']);
            $list['list'][$k]['info'] = $info;
            $comment = jtable('mall_comment')->info(array('id'=>$one['comment_id']));
            $list['list'][$k]['comment'] = $comment;
        }  
        $express = '';
        if(is_array($list['list'])){
            if($list['list'][0]['status'] == '2'){
                $express = jclass('express')->getorder($list['list'][0]['logisticsname'],$list['list'][0]['logisticsid']);
            }
            
        }
        $json['express'] = $express;
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['list'] = $list;
        json_result('加载成功', $json);
    }
    function BuyNow(){
        $uid = jget('uid','int');
        $id = jget('id','int');
        $num = jget('num','int');
        if ($uid < 1) {
            json_error("用户不存在");
        }
        if ($id < 1) {
            json_error("商品不存在");
        }
        if ($num < 1) {
            json_error("请选择数量");
        }
        $mall = jlogic("mall")->get_info($id);
        if(is_array($mall)){
            $gold = $mall['gold'];
            $member = jtable("members")->info($uid);
            if(is_array($member)){
                if($gold*$num > $member['extcredits2']){
                    $ratio = 1;
                    $credits = jconf::get('credits');
                    if ($credits['recharge_ratio']) {
                        $extcredits2 = 1;
                        if($member['extcredits2'] == 0){
                            $credits['recharge_ratio'] = str_replace(array('$member[extcredits2]',), array( '$extcredits2',), $credits['recharge_ratio']);
                        }else{
                            $extcredits2 = $member[extcredits2];
                        }
                        eval("\$real_price = ".$credits['recharge_ratio'].";");
                        $ratio = $real_price/$extcredits2;
                    }
                    $diff = ($gold*$num - $member['extcredits2'])*$ratio;
                    
                    //$message = "总".$credits['ext']['extcredits2']['name'].$gold*$num.",您现有".$member['extcredits2'].$credits['ext']['extcredits2']['name'].'，另需支付'.$diff.'元，';
                    $message = "总".$credits['ext']['extcredits2']['name'].$gold*$num.",您现有".$member['extcredits2'].$credits['ext']['extcredits2']['name'].'，';
                    $data['diff'] = $diff;
                    $data['message'] = $message;
                    json_error("您的".$credits['ext']['extcredits2']['name']."不足",$data);
                }else{
                    json_result('结算中');
                }
            }else{
                json_error("用户不存在");
            }
            
        }else{
            json_error("商品不存在");
        }
    }
    function FictitiousOrder(){
        $uid = jget('uid','int');
        $id = jget('id','int');
        $num = jget('num','int');
        if ($uid < 1) {
            json_error("用户不存在");
        }
        if ($id < 1) {
            json_error("商品不存在");
        }
        if ($num < 1) {
            json_error("请选择数量");
        }
        $mall = jlogic("mall")->get_info($id);
        if(is_array($mall)){
            $gold = $mall['gold'];
            $member = jtable("members")->info($uid);
            if(is_array($member)){
                if($gold*$num > $member['extcredits2']){
                    $ratio = 1;
                    $credits = jconf::get('credits');
                    if ($credits['recharge_ratio']) {
                        $extcredits2 = 1;
                        if($member['extcredits2'] == 0){
                            $credits['recharge_ratio'] = str_replace(array('$member[extcredits2]',), array( '$extcredits2',), $credits['recharge_ratio']);
                        }else{
                            $extcredits2 = $member[extcredits2];
                        }
                        eval("\$real_price = ".$credits['recharge_ratio'].";");
                        $ratio = $real_price/$extcredits2;
                    }
                    $diff = ($gold*$num - $member['extcredits2'])*$ratio;
                    $data['mall'] = $mall;
                    $data['site_url'] = $GLOBALS['_J']['site_url'];
                    $data['price'] = $diff;
                    $data['gold'] = $member['extcredits2'];
                    $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
                    $data['address'] = $address;
                    json_result("结算中",$data);
                }else{
                    $data['mall'] = $mall;
                    $data['site_url'] = $GLOBALS['_J']['site_url'];
                    $data['price'] = '0.00';
                    $data['gold'] = $gold*$num;
                    $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
                    $data['address'] = $address;
                    json_result("结算中",$data);
                }
            }else{
                json_error("用户不存在");
            }
            
        }else{
            json_error("商品不存在");
        }
    }
    /**
     * 添加订单
     */
    function AddFictitiousOrder() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        //$receiver_tel = jget('receiver_tel', 'mobile');
        //$receiver_name = trim(jget('receiver_name'),'txt');
        $pay_type = jget('pay_type');
        $pro_id = jget('pro_id','int');
        $pro_num = jget('pro_num','int');
        $gold_total = jget('gold_total');
        $amount = jget('amount');
//        if($receiver_name == ''){
//            return json_error('请填写收货人姓名');
//        }
//        
//        if (empty($receiver_tel)) {
//            return json_error('请填写正确的手机号码');
//        }
        if($pro_id < 1){
            return json_error('商品不存在！');
        }
        $info = jlogic("mall")->get_info($pro_id);
        
        $member = jsg_member_info($uid);
        $ordersid = date('YmdHis', time()) . mt_rand(1000, 9999);
        if(is_array($info)) {
            
            $data = array(
                'uid' => $uid,
                'username' => $member['nickname'],
                'ordersid' => $ordersid,
                'goods_id' => $info['id'],
                'goods_name' => $info['name'],
                'goods_num' => $pro_num,
                'goods_price' => $info['price'],
                'goods_gold' => $info['gold'],
                'goods_style' => $info['style'],
                'pay_price' => $amount,
                'pay_gold' => $gold_total,
                //'receiver_name' => $receiver_name,
                //'receiver_tel' => $receiver_tel,
                'pay_type' => $pay_type,
                'add_time' => TIMESTAMP,
                'pay_time' => TIMESTAMP,
                'status' => 1,
            );
            if($amount > 0){
                $data['status'] = 0;
            }else{
                jtable('members')->update(array('extcredits2'=>($member['extcredits2']-$gold_total)),array('uid'=>$uid));
                update_credits_by_action('convert', $uid);
                update_credits_by_action('consume',$uid);
                $gifts = jtable('my_gifts')->info(array('uid'=>$uid,'gid'=>$pro_id));
                if(is_array($gifts)){
                    $savedata = array(
                        'uid'=>$uid,
                        'gid'=>$pro_id,
                        'num' => $gifts['num'] + $pro_num,
                    );
                    jtable('my_gifts')->update($savedata,array('uid'=>$uid,'gid'=>$pro_id));
                }else{
                    $savedata = array(
                        'uid'=>$uid,
                        'gid'=>$pro_id,
                        'num' => $pro_num,
                    );
                    jtable('my_gifts')->insert($savedata);
                }
            }
            jlogic('mall_order')->add_order($data);
            $starttime = strtotime(date('Y-m-d 00:00:00', time()));
            $endtime = strtotime(date('Y-m-d 23:59:59', time()));
            $where_arr = array(
                'uid' => $uid,
                'goods_style' => 'fictitious',
                '>=@pay_time' => $starttime,
                '<=@pay_time' => $endtime,
                '>=@status' => 1,
                '<=@status' => 2
            );
            $count = jtable('mall_order')->count($where_arr);
            if($count >= 10){
                update_credits_by_action('convert-ten',$uid);
            }
//            $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
//            if(is_array($address)){
//                $address_data = array(
//                    'receiver_uid' => $uid,
//                    'receiver_name' => $receiver_name,
//                    'receiver_tel' => $receiver_tel
//                );
//                jtable('mall_address')->update($address_data,array('id'=>$address['id']));
//            }else{
//                $address_data = array(
//                    'receiver_uid' => $uid,
//                    'receiver_name' => $receiver_name,
//                    'receiver_tel' => $receiver_tel,
//                    'addtime' => date('Y-m-d H:i:s', time())
//                );
//                jtable('mall_address')->insert($address_data);
//            }
            $json['ordersid'] = $ordersid;
            return json_result('礼物兑换成功',$json);
        }else{
            return json_error('商品不存在！');
        }
        
    }
    public function DoCollect(){
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $gid = jget('gid','int');
        if ($gid < 1) {
            json_error("商品不存在");
        }
        $info = jtable('mall_collect')->get(array('c_uid'=>$uid,'gid'=>$gid));
        if(count($info)>0){
            json_result("这个商品您已经收藏了");
        }else{
            $savedata = array(
                'c_uid' => $uid,
                'gid' => $gid,
                'addtime' => date('Y-m-d H:i:s', time())
            );
            jtable('mall_collect')->insert($savedata);
            return json_result('收藏成功');
        }
    }
    /**
     * 收藏列表
     */
    function CollectList() {
        $MallLogic = jlogic('mall');
        $uid = jget('uid','int');
        $page = jget('page','int');
        if($page < 1){
            $page = 1;
        }
        if($uid < 1){
            json_error("用户不存在");
        }
        $where = " and c_uid='$uid'";
        $list = $MallLogic->get_collect_list($where,$page);
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['list'] = $list;
        json_result('加载成功', $json);
    }
    public function DelCollect(){
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $cid = jget('cid','int');
        if ($cid < 1) {
            json_error("这个收藏不存在");
        }

        jtable('mall_collect')->delete(array('id'=>$cid));
        return json_result('删除成功');
    }

    public function MyAdress() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
        $json['address'] = $address;
        json_result('加载成功', $json);
    }
    
    public function SaveAdress() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $receiver_address = trim(jget('address', 'txt'));
        $receiver_tel = jget('receiver_tel', 'mobile');
        $receiver_name = trim(jget('receiver_name'),'txt');
        $receiver_state = trim(jget('receiver_state'),'txt');
        $receiver_city = trim(jget('receiver_city'),'txt');
        $receiver_district = trim(jget('receiver_district'),'txt');
        $postcode = trim(jget('postcode'),'txt');
        $address = jtable('mall_address')->info(array('receiver_uid'=>$uid));
        if(is_array($address)){
            $address_data = array(
                'receiver_uid' => $uid,
                'receiver_name' => $receiver_name,
                'receiver_tel' => $receiver_tel,
                'receiver_state' => $receiver_state,
                'receiver_city' => $receiver_city,
                'receiver_district' => $receiver_district,
                'receiver_address' => $receiver_address,
                'receiver_postcode' => $postcode
            );
            jtable('mall_address')->update($address_data,array('id'=>$address['id']));
        }else{
            $address_data = array(
                'receiver_uid' => $uid,
                'receiver_name' => $receiver_name,
                'receiver_tel' => $receiver_tel,
                'receiver_state' => $receiver_state,
                'receiver_city' => $receiver_city,
                'receiver_district' => $receiver_district,
                'receiver_address' => $address,
                'receiver_postcode' => $postcode,
                'addtime' => date('Y-m-d H:i:s', time())
            );
            jtable('mall_address')->insert($address_data);
        }
        json_result('保存成功');
    }
    public function RechargeList() {
        $page = jget('page','int');
        if(!isset($page)){
            $page = 1;
        }
        $page_size = 20;
        $where = '';
        $order = 'amount asc'; 
        $list = jlogic("recharge")->get_list_bypage($where,$page,$page_size,$order);
        $data['lists'] = $list;
        json_result('加载成功', $data);
    }
    function Recharge() {
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $pay_type = jget('pay_type');
        $id = jget('id','int');
        $amount = jget('amount');

        if($id < 1){
            return json_error('商品不存在！');
        }
        $info = jtable("recharge")->info($id);
        
        $member = jsg_member_info($uid);
        $ordersid = date('YmdHis', time()) . mt_rand(1000, 9999);
        if(is_array($info)) {
            
            $data = array(
                'ordersid' => $ordersid,
                'r_uid' => $uid,
                'username' => $member['nickname'],
                'pay_price' => $amount,
                'pay_type' => $pay_type,
                'addtime' => date('Y-m-d H:i:s', time()),
                'amount' => $info['amount'],
                'get_coin' => $info['get_coin'],
                'r_id' => $info['id'],
                'pay_status' => 1
            );
            if($amount > 0){
                $data['pay_status'] = 0;
            }else{
                $data['pay_time'] = date('Y-m-d H:i:s', time());
                jtable('members')->update(array('extcredits2'=>($member['extcredits2']+$info['get_coin'])),array('uid'=>$uid));
                update_credits_by_action('recharge', $uid);
                update_credits_by_action('recharging', $uid);
            }
            jtable('recharge_log')->insert($data);
            $starttime = date('Y-m-d 00:00:00', time());
            $endtime = date('Y-m-d 23:59:59', time());
            $where_arr = array(
                'r_uid' => $uid,
                '>=@pay_time' => $starttime,
                '<=@pay_time' => $endtime,
                'pay_status' => '1'
            );
            $count = jtable('recharge_log')->count($where_arr);
            if($count >= 5){
                update_credits_by_action('recharge-five',$uid);
            }
            $json['ordersid'] = $ordersid;
            return json_result('正在结算',$json);
        }else{
            return json_error('商品不存在！');
        }
        
    }
    public function AddComment(){
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $oid = jget('oid');
        $score = jget('score','int');
        $content = jget('content');
        $path = jget('path');
        $strcids = str_replace('\\', '', $path);
        //$strcids = preg_replace('/[\"\[\]]/', '', $path);
        //$strcidarr = explode(',', $strcids);
        $order = jtable('mall_order')->info(array('ordersid'=>$oid));
        if(is_array($order)){
            $savedata = array(
                'uid' => $uid,
                'ordersid' => $oid,
                'gid'=>$order['goods_id'],
                'username' => $order['username'],
                'content' => $content,
                'score' => $score,
                'path' => $strcids,
                'addtime' => date('Y-m-d H:i:s', time())
            );
            $insert_id = jtable('mall_comment')->insert($savedata,1);
            jtable('mall_order')->update(array('comment_id'=>$insert_id),array('ordersid'=>$oid));
            return json_result('评价成功');
        }else{
             return json_error('订单不存在！');
        }
    }
    /**
     * 收藏列表
     */
    function CommentList() {
        $MallLogic = jlogic('mall');
        $gid = jget('gid','int');
        $page = jget('page','int');
        if($page < 1){
            $page = 1;
        }
        if($gid < 1){
            json_error("商品不存在");
        }
        $where = " and gid='$gid'";
        $list = $MallLogic->get_comment_list($where,$page);
        $json['site_url'] = $GLOBALS['_J']['site_url'];
        $json['list'] = $list;
        json_result('加载成功', $json);
    }
    /**
     * 意见和建议
     * @return type
     */
    public function AddSuggest(){
        $uid = jget('uid','int');
        if ($uid < 1) {
            json_error("请登录");
        }
        $content = jget('content');
        $path = jget('path');
        $strcids = str_replace('\\', '', $path);
        $member = jsg_member_info($uid);
        $savedata = array(
            'uid' => $uid,
            'username' => $member['nickname'],
            'content' => $content,
            'path' => $strcids,
            'addtime' => date('Y-m-d H:i:s', time())
        );
        jtable('suggest')->insert($savedata,1);
        return json_result('提交成功');
    }
    function CheckPay() {
        $is_show = 'false';
        $data['is_show'] = $is_show;
        json_result('加载成功',$data);
    }
}

?>