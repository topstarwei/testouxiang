<?php

/**
 * 文件名：member.mod.php
 * @version $Id: member.mod.php 5501 2014-01-23 02:28:27Z chenxianfeng $
 * 作者：狐狸<foxis@qq.com>
 * 功能描述: 注册验证模块，已经统一到member.func.php文件中的函数进行验证
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject(& $config) {
        $this->MasterObject($config);


        $this->Execute();
    }

    function Execute() {
        ob_start();
//        jsp_ckeck_signature();
//        $uncheckdata = array('checkpay','checkwx','login','doregister','sendsms','verifycode','sinaauthcallback','qqauthcallback','wxauthcallback');
//        if(!in_array($this->Code,$uncheckdata)){
//            jsp_login_check();
//        }
        switch ($this->Code) {
            case 'index':
                $this->Index();
                break;
            case 'login':
                $this->Login();
                break;
            case 'doregister':
                $this->DoRegister();
                break;
            case 'sendsms':
                $this->Sendsms();
                break;
            case 'verifycode':
                $this->Verifycode();
                break;
            case 'setpwd':
                $this->Setpwd();
                break;
            case 'uploadfile':
                $this->Uploadfile();
                break;
            case 'getuserinfo':
                $this->Getuserinfo();
                break;
            case 'getstarinfo':
                $this->Getstarinfo();
                break;
            case 'do_modify_profile':
                $this->DoModifyProfile();
                break;
            case 'dynamic_my':
                $this->Dynamicmy();
                break;
            case 'userinfo':
                $this->Userinfo();
                break;
            case 'getmemberlist':
                $this->Getmemberlist();
                break;
            case 'get_dynamic':
                $this->GetDynamic();
                break;
            case 'dig':
                $this->Dig();
                break;
            case 'forward':
                $this->Do_forward();
                break;
            case 'sendgifts':
                $this->SendGifts();
                break;
            case 'mygifts':
                $this->MyGifts();
                break;
            case 'mysendgifts':
                $this->MySendGifts();
                break;
            case 'dosign':
                $this->DoSign();
                break;
            case 'rankgifts':
                $this->RankGifts();
                break;
            case 'grabredenvelopetotal': {
                    $this->GetRedEnvelopeTotal();
                    break;
                }
            case 'grabredenvelope': {
                    $this->GrabRedEnvelope();
                    break;
                }
            case 'qhb': {
                    $this->QHB();
                    break;
                }
            case 'grabredenvelopelog': {
                    $this->GrabRedEnvelopeLog();
                    break;
                }
            case 'getpmlist': {
                    $this->GetPmList();
                    break;
                }
            case 'interlocution': {
                    $this->Interlocution();
                    break;
                }
            case 'dointerlocution': {
                    $this->DoInterlocution();
                    break;
                }
            case 'webinfo':
                $this->WebInfo();
                break;
            case 'reply':
                $this->Reply();
                break;
            case 'totlesms':
                $this->TotleSms();
                break;
            case 'addbarrage':
                $this->AddBarrage();
                break;
            case 'listbarrage':
                $this->ListBarrage();
                break;
            case 'dailytasks':
                $this->DailyTasks();
                break;
            case 'audition':
                $this->Audition();
                break;
            case 'auditiondetail':
                $this->AuditionDetail();
                break;
            case 'doaudition':
                $this->DoAudition();
                break;
            case 'look':
                $this->Look();
                break;
            case 'lookdetail':
                $this->LookDetail();
                break;
            case 'dolook':
                $this->DoLook();
                break;
            case 'sinaauthcallback':
                $this->SinaAuthCallback();
                break;
            case 'qqauthcallback':
                $this->QqAuthCallback();
                break;
            case 'wxauthcallback':
                $this->WxAuthCallback();
                break;
            case 'getreplylist':
                $this->GetReplyList();
                break;
            case 'getdiglist':
                $this->GetDigList();
                break;
            case 'getactivitylist':
                $this->GetActivityList();
                break;
            case 'getarticlelist':
                $this->GetArticleList();
                break;
            case 'topic_reply':
                $this->TopicReply();
                break;
            case 'my_follows':
                $this->MyFollows();
                break;
            case 'checkwx':
                $this->CheckWx();
                break;
            case 'demo':
                $this->Demo();
                break;
            default:
                $this->Main();
                break;
        }
        response_text(ob_get_clean());
    }

    function Main() {
        response_text("正在建设中……");
    }

    /**
     * 首页
     */
    function Index() {
        $slide_list = array();
        $slide_config = jconf::get('slide_index');
        if ($slide_config) {
            foreach ($slide_config['list'] as $key => $value) {
                if (!stristr($value['src'], 'http://') && !stristr($value['src'], 'https://')) {
                    $slide_config['list'][$key]['src'] = $GLOBALS['_J']['site_url'] . '/' . $value['src'];
                }
            }
            $slide_list = $slide_config['list'];
        }
        $sql = "select a.*,b.foreign_name,b.alias from " . DB::table('members') . " a left join " . DB::table('members_profile') . " b  on a.uid = b.uid  where `role_id` = 119 order by fans_count desc limit 10";
        $list = DB::fetch_all($sql);
        if (count($list) > 0) {
            foreach ($list as $key => $value) {
                $value = jsg_member_make($value);
                $list[$key] = $value;
            }
        }
        $json['members_list'] = $list;
        $json['slide_list'] = $slide_list;
        json_result('加载成功', $json);
    }

    /**
     * 登陆
     */
    function Login() {
        $phone = trim(jget('username'));
        $password = jget('password');

        if ($phone == "" || $password == "") {
            json_error("无法登录,手机号或密码不能为空");
        }

        $rets = jsg_member_login($phone, $password, 'phone');
        $uid = (int) $rets['uid'];
        if ($uid < 1) {
            json_error($rets['error']);
        }

        $member = jsg_member_info($uid, 'uid');
        $options['uid'] = array($uid);
        $mydigout_list = $this->GetTypeTid('mydigout', $uid, $options);
        $my_reply_list = $this->GetTypeTid('my_reply', $uid, $options);
        $forward_list = $this->GetTopic($uid, $options);
        $member['topic_count'] = count($mydigout_list) + count($my_reply_list) + count($forward_list);
        $member['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('登录成功', $member);
    }

    function login_info($uid) {
        $member = jsg_member_info($uid, 'uid');
        $options['uid'] = array($uid);
        $mydigout_list = $this->GetTypeTid('mydigout', $uid, $options);
        $my_reply_list = $this->GetTypeTid('my_reply', $uid, $options);
        $forward_list = $this->GetTopic($uid, $options);
        $member['topic_count'] = count($mydigout_list) + count($my_reply_list) + count($forward_list);
        $member['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('登录成功', $member);
    }

    /**
     * 注册
     */
    function DoRegister() {
        $regstatus = jsg_member_register_check_status();
        if ($regstatus['error']) {
            json_error($regstatus['error']);
        }

        $message = array();
        $timestamp = time();

        $inviter_member = array();
        $invite_code = ($this->Post['invite_code'] ? $this->Post['invite_code'] : $this->Get['invite_code']);
        $check_result = jsg_member_register_check_invite($invite_code);

        if ($regstatus['invite_enable'] && !$regstatus['normal_enable']) {
            if (!$invite_code) {
                json_error("本站目前需要有好友邀请链接才能注册");
            }

            if (!$check_result) {
                json_error("对不起，您访问的邀请链接不正确或者因邀请数已满而失效，请重新与邀请人索取链接。");
            }
        }

        if ($check_result['uid'] > 0) {
            $inviter_member = jsg_member_info($check_result['uid']);
        }
        if (!$inviter_member && $this->Config['register_invite_input']) {
            $inviter_member = jsg_member_info($this->Post['inviter_nickname'], 'nickname');
        }

        $password = jget('password');
        $phone = jget('phone');
        $nickname = jget('nickname');
        $email = jget('email');


        if (strlen($password) < 5) {
            json_error("密码过短，请设置至少5位");
        }
        if ($password != jget('password2')) {
            json_error("两次输入的密码不相同");
        }
        $code = jget('vcode');
        $type = jget('type');
        $ret = jsg_member_checkphone($phone);
        if ($type == 'register') {
            if ($ret == '-9') {
                json_error("此号码已注册");
            } else if ($ret == '-8') {
                json_error("手机号不合法");
            }
        } else {
            if ($ret == 1) {
                json_error("此号码未注册");
            } else if ($ret == '-8') {
                json_error("手机号不合法");
            }
        }
        if ($code == '') {
            json_error('验证码不能为空');
        }
        $sql_datas = array(
            'phone' => $phone,
            'code' => $code,
            'sms_type' => $type
        );
        $result = jtable('emay_sms')->row($sql_datas);
        if (count($result) > 0) {
            $list = $result['list'];
            $sendtime = $list[0]['sendtime'];
            $nowtime = time();
            if (($nowtime - strtotime($sendtime)) > 300) {
                json_error("验证码已失效");
            }
        } else {
            json_error('验证码不正确');
        }

        $uid = jsg_member_register_by_phone($nickname, $password, $phone, $email);
        if ($uid < 1) {
            $regconf = jconf::get('register');
            $rets = array(
                '0' => '【注册失败】有可能是站点关闭了注册功能',
                '-1' => '帐户/昵称 不合法，含有不允许注册的字符，请尝试更换一个。',
                '-2' => '帐户/昵称 不允许注册，含有被保留的字符，请尝试更换一个。',
                '-3' => '帐户/昵称 已经存在了，请尝试更换一个。',
                '-4' => 'Email 不合法，请输入正确的Email地址。',
                '-5' => 'Email 不允许注册，请尝试更换一个。',
                '-6' => 'Email 已经存在了，请尝试更换一个。',
                '-8' => '手机号不合法，请输入正确的手机号。',
                '-9' => '手机号 已经存在了，请尝试更换一个。',
                '-7' => '您的IP地址 ' . $GLOBALS['_J']['client_ip'] . ' 已经被限制注册了（一个IP地址 ' . $regconf['time_html'] . ' 之内，最多只能注册 ' . $regconf['limit'] . ' 个用户），请稍后再试或联系管理员',
            );
            json_error($rets[$uid]);
        }
        $rets = jsg_member_login($uid, $password, 'uid');
        json_result('注册成功', $rets);
    }

    public function Demo() {
        $uid = '10';
        $member = jsg_member_info($uid, 'uid');
        $RankGiftsLogic = jlogic('rank_gifts');
        $m_total = 0;
        $m_rs = $RankGiftsLogic->get_rankgifts_bygroup(" and to_uid='$uid'", ' group by to_uid');
        if ($m_rs) {
            $m_total = $m_rs['rank_num'];
        }
        $member['m_total'] = $m_total;
        $member['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('用户详情', $member);
    }
    function microtime_float(){
        list($t1, $t2) = explode(' ', microtime());
        return (float)sprintf('%.0f',(floatval($t1)+floatval($t2))*1000);
    }
    public function TotleSms() {
        $rs = jclass('emay_sms')->totlesms();
        var_dump($rs);
    }

    //发送短信
    function Sendsms() {
        $phone = jget('phone');
        $type = jget('type');
        $ret = jsg_member_checkphone($phone);
        if ($type == 'register') {
            if ($ret == '-9') {
                json_error("此号码已注册");
            } else if ($ret == '-8') {
                json_error("手机号不合法");
            }
        } else {
            if ($ret == 1) {
                json_error("此号码未注册");
            } else if ($ret == '-8') {
                json_error("手机号不合法");
            }
        }

        $vet = jsg_member_verify_tel($phone, $type);
        $vets = array(
            '0' => '手机号为空',
            '-1' => '每天只能发送三次'
        );
        if ($vet < 1) {
            json_error($vets[$vet]);
        }
        $code = jsg_member_randomkeys(4);
        if ($type == 'register') {
            $content = "【摩骚SOMALL】" . $code . "为您的注册验证码，请在5分钟内完成验证。如非本人操作，请忽略或回复T退订。";
        } else {
            $content = "【摩骚SOMALL】" . $code . "为您的找回密码验证码，请在5分钟内完成验证。如非本人操作，请忽略或回复T退订。";
        }
        $rs = jsg_member_sendSMS(array($phone), $content);
        if ($rs) {
            $sql_datas = array(
                'phone' => $phone,
                'code' => $code,
                'sms_type' => $type,
                'sendtime' => date('Y-m-d H:i:s', time())
            );
            jtable('emay_sms')->insert($sql_datas, 1);
            json_result('发送成功');
        } else {
            json_error('发送失败');
        }
    }

    //验证码验证
    function Verifycode() {
        $phone = jget('phone');
        $code = jget('vcode');
        $type = jget('type');
        $ret = jsg_member_checkphone($phone);
        if ($type == 'register') {
            if ($ret == '-9') {
                json_error("此号码已注册");
            } else if ($ret == '-8') {
                json_error("手机号不合法");
            }
        } else {
            if ($ret == 1) {
                json_error("此号码未注册");
            } else if ($ret == '-8') {
                json_error("手机号不合法");
            }
        }
        if ($code == '') {
            json_error('验证码不能为空');
        }
        $sql_datas = array(
            'phone' => $phone,
            'code' => $code,
            'sms_type' => $type
        );
        $result = jtable('emay_sms')->row($sql_datas);
        if (count($result) > 0) {
            $list = $result['list'];
            $sendtime = $list[0]['sendtime'];
            $nowtime = time();
            if (($nowtime - strtotime($sendtime)) < 300) {
                $member = jsg_member_info($phone, 'phone', '*');
                $member['site_url'] = $GLOBALS['_J']['site_url'];
                json_result('验证成功', $member);
            } else {
                json_error("验证码已失效");
            }
        } else {
            json_error('验证码不正确');
        }
    }

    //修改密码
    function Setpwd() {
        $uid = jget('uid');
        $password = jget('password');
        $password2 = jget('password2');
        if ($password != $password2 or $password == '') {
            json_error("两次输入的密码不一致,或新密码不能为空。");
        }
        $member_info = jsg_member_info($uid);
        if (!$member_info) {
            json_error("用户不存在");
        }
        $ret = jsg_member_edit($member_info['nickname'], '', '', $password, '', '', 1);
        if ($ret < 1) {
            json_error("不能和原密码一样");
        }
        json_result('重置成功', $ret);
    }

    function getname($exname) {
        $dir = "./images/uploadfile/";
        $i = 1;
        if (!is_dir($dir)) {
            mkdir($dir, 0777);
        }
        while (true) {
            if (!is_file($dir . $i . "." . $exname)) {
                $name = $i . "." . $exname;
                break;
            }
            $i++;
        }
        return $dir . $name;
    }

    /**
     * 上传头像
     */
    function Uploadfile() {
        $base64 = jget('base64');
        $strdata = explode(',', $base64);
        $clearbase64 = $strdata[1];
        $IMG = base64_decode($clearbase64);
        $uid = jget('uid');
        $image_path = RELATIVE_ROOT_PATH . 'images/' . ($_J['config']['face_verify'] ? 'face_verify' : 'face') . '/' . face_path($uid);
        $image_name = $uid . '_b.jpg';
        $image_file = $image_path . $image_name;
        if (!is_dir($image_path)) {
            jmkdir($image_path);
        }
        file_put_contents($image_file, $IMG);
        jlogic('image')->face_upload($image_file, $uid);
        $sys_config = jconf::get();
        if ($sys_config['ftp_on']) {
            $ftp_key = randgetftp();
            $get_ftps = jconf::get('ftp');
            $site_url = $get_ftps[$ftp_key]['attachurl'];
            $image_file = $attach_file = $site_url . '/' . str_replace('./', '', $image_file);
        }
        $sql = "update `" . TABLE_PREFIX . "members` set `face`='{$image_file}' where `uid`='$uid'";
        DB::query($sql);
        json_result('上传成功');
    }

    /**
     * 获取用户信息
     */
    function Getuserinfo() {
        $uid = jget('uid');
        $member = jsg_member_info($uid, 'uid');
        $options['uid'] = array($uid);
        $mydigout_list = $this->GetTypeTid('mydigout', $uid, $options);
        $my_reply_list = $this->GetTypeTid('my_reply', $uid, $options);
        $forward_list = $this->GetTopic($uid, $options);
        $member['topic_count'] = count($mydigout_list) + count($my_reply_list) + count($forward_list);
        $member['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('用户详情', $member);
    }

    /**
     * 获取明星粉丝微博信息
     */
    function Getstarinfo() {
        $uid = jget('uid');
        $member = jsg_member_info($uid, 'uid');
        $RankGiftsLogic = jlogic('rank_gifts');
        $m_total = 0;
        $m_rs = $RankGiftsLogic->get_rankgifts_bygroup(" and to_uid='$uid'", ' group by to_uid');
        if ($m_rs) {
            $m_total = $m_rs['rank_num'];
        }
        $member['m_total'] = $m_total;
        $member['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('用户详情', $member);
    }

    /**
     * 修改用户信息
     */
    function DoModifyProfile() {
        $uid = jget('uid');
        $nickname = jget('nickname');
        $signature = jget('signature');
        $phone = jget('phone');
        $sex = jget('sex');
        $street = jget('street');
        $province = jget('province');
        $city = jget('city');
        $area = jget('area');
        $birthday = jget('birthday');

        $member_info = jsg_get_member($uid, 'uid');
        if (!$member_info) {
            json_error("用户不存在");
        }
        $nickname_num = jtable('members')->count(array('nickname' => $nickname, '<>@uid' => $uid));
        if ($nickname_num > 0) {
            json_error("昵称已经存在了，请尝试更换一个");
        }
        $phone_ret = jsg_member_checkphone($phone);
        if ($phone_ret == '-8') {
            json_error('手机号不合法');
        }
        $phone_num = jtable('members')->count(array('phone' => $phone, '<>@uid' => $uid));
        if ($phone_num > 0) {
            json_error("手机号已经存在了，请尝试更换一个");
        }
        $newmember = array(
            'nickname' => $nickname,
            'signature' => $signature,
            'phone' => $phone,
            'gender' => $sex,
            'bday' => $birthday,
            'street' => $street,
            'province' => $province,
            'city' => $city,
            'area' => $area
        );
        jtable('members')->update($newmember, array('uid' => $uid));
        json_result('修改成功');
    }

    /**
     * 获取我的动态
     */
    function Dynamicmy() {
        $uid = jget('uid');
        $dateline = jget('dateline');
        //$TopicListLogic = jlogic('topic_list');
        $options['perpage'] = '20';
        $options['uid'] = array($uid);
        $options['dateline'] = $dateline;
        $mydigout_list = $this->GetTypeTid('mydigout', $uid, $options);
        $my_reply_list = $this->GetTypeTid('my_reply', $uid, $options);
        $forward_list = $this->GetTopic($uid, $options);
        if (count($mydigout_list) > 0) {
            foreach ($mydigout_list as $key => $value) {
                $mydigout_list[$key] = $value;
                $mydigout_list[$key]['topic_typeid'] = $value['topic_type'];
                $mydigout_list[$key]['topic_type'] = 'mydigout';
            }
        }
        if (count($my_reply_list) > 0) {
            foreach ($my_reply_list as $key => $value) {
                $my_reply_list[$key] = $value;
                $my_reply_list[$key]['topic_typeid'] = $value['topic_type'];
                $my_reply_list[$key]['topic_type'] = 'my_reply';
            }
        }
        if (count($forward_list) > 0) {
            foreach ($forward_list as $key => $value) {
                $forward_list[$key] = $value;
                $forward_list[$key]['topic_typeid'] = $value['topic_type'];
                $forward_list[$key]['topic_type'] = 'forward';
            }
        }
        //$array = array_merge($mydigout_list,$my_reply_list,$forward_list);
        $array = array_merge($mydigout_list, $my_reply_list, $forward_list);
        $dates = array();
        foreach ($array as $val) {
            $dates[] = $val['addtime'];
        }
        array_multisort($dates, SORT_DESC, $array);
        $my_reply_data = $this->TopicLogic->GetParentTopic($my_reply_list, ('mycomment' == 'my_reply'));
        if (count($my_reply_data) > 0) {
            foreach ($my_reply_data as $key => $value) {
                $my_reply_data[$key]['topic_typeid'] = $value['topic_type'];
            }
        }
        $forward_data = $this->TopicLogic->GetParentTopic($forward_list, ('mycomment' == ''));
        if (count($forward_data) > 0) {
            foreach ($forward_data as $key => $value) {
                $forward_data[$key]['topic_typeid'] = $value['topic_type'];
            }
        }
        $data['my_reply_data'] = $my_reply_data;
        $data['forward_data'] = $forward_data;
        $data['topic_list'] = $array;
        if (count($array) > 0) {
            $num = count($array) - 1;
            $dateline = $array[$num]['addtime'];
            $data['dateline'] = $dateline;
            json_result('加载成功', $data);
        } else {
            json_error("没有更多的数据了");
        }
    }

    function GetTypeTid($dtype, $uid, $options) {
        $TopicListLogic = jlogic('topic_list');
        $topic_list = array();
        if ('my_reply' == $dtype) {
            $type = $dtype;
            $options['type'] = array('reply', 'both');
        }
        $type = $dtype;
        $options['get_list'] = 1;
        $getTypeTidReturn = $TopicListLogic->GetTypeTid($dtype, $uid, $options);
        if (isset($getTypeTidReturn['list'])) {
            $topic_list = $getTypeTidReturn['list'];
        }
        return $topic_list;
    }

    function GetTopic($uid, $options) {
        if (isset($options['dateline']) && !empty($options['dateline'])) {
            $options['<@dateline'] = $options['dateline'];
        }
        unset($options['dateline']);
        $p = $options;
        $p['type'] = array('first', 'forward', 'both');

        if (!$p['sql_order']) {
            $p['sql_order'] = ' `dateline` DESC ';
        }
        $_rets = jtable('member_topic')->get_tids($uid, $p, 1);
        $topic_list = $this->TopicLogic->Get($_rets['ids']);
        return $topic_list;
    }

    /**
     * 获取明星信息
     */
    function Userinfo() {
        $uid = jget('uid');
        if (!isset($uid) && empty($uid)) {
            json_error("用户不存在");
        }
        $rs = jtable('members_profile')->get(array('uid' => $uid));
        $rs['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('加载成功', $rs);
    }

    /**
     * 获取明星列表
     */
    function Getmemberlist() {
        $where = array();
        $MemberLogic = jlogic('member');
        $page = jget('page');
        if (!isset($page)) {
            $page = 1;
        }
        $sex = jget('sex');
        if (isset($sex) && $sex != '') {
            $where['gender'] = $sex;
        }
        $name = jget('name');
        if (isset($name) && $name != '') {
            $where['nickname'] = $name;
        }
        $initial = jget('initial');
        if (isset($initial) && $initial != '') {
            $where['initial'] = $initial;
        }
        $where['role_id'] = '119';
        $pagesize = 25;
        $order_by = 'fans_count desc';
        $uid = jget('uid');
        $lists = $MemberLogic->get_member_list($where, $page, $pagesize, $order_by);
        $rets = buddy_follow($lists, 'uid', $uid);
        json_result('加载成功', $rets);
    }

    /**
     * 获取明星动态列表
     */
    function GetDynamic() {
        $uid = jget('uid');
        $mid = jget('mid', 'int');
        $type = jget('type');
        $dateline = jget('dateline');
        $options['perpage'] = '20';
        $options['uid'] = array($uid);
        $options['dateline'] = $dateline;
        $options['topic_type'] = '0';
        if ($type == 'pic') {
            $options['<>@imageid'] = '';
        } else if ($type == 'video') {
            $options['<>@videoid'] = 0;
        } else if ($type == 'article') {
            $options['imageid'] = '';
            $options['videoid'] = '';
        }
        if (isset($options['dateline']) && !empty($options['dateline'])) {
            $options['<@dateline'] = $options['dateline'];
        }
        unset($options['dateline']);
        $p = $options;
        $p['type'] = array('first', 'forward');

        if (!$p['sql_order']) {
            $p['sql_order'] = ' `dateline` DESC ';
        }
        $datetime = date('Y-m-d H:i:s', time());
        $envelope_info = jtable('grab_red_envelope')->info(array('uid' => $uid, 'status' => 1, '<=@stime' => $datetime, '>=@etime' => $datetime));
        $data['envelope'] = $envelope_info;
        $_rets = jtable('member_topic')->get_tids($uid, $p, 1);
        $topic_list = $this->TopicLogic->Get($_rets['ids']);
        $dates = array();
        foreach ($topic_list as $key => $val) {
            $count = DB::result_first("SELECT COUNT(*) FROM " . DB::table('topic_dig') . " WHERE tid='{$val['tid']}' AND uid = '" . $mid . "'");
            $topic_list[$key]['is_dig'] = '0';
            if ($count > 0) {
                $topic_list[$key]['is_dig'] = '1';
            }
            $dates[] = $val['addtime'];
        }
        array_multisort($dates, SORT_DESC, $topic_list);
        if (count($topic_list) > 0) {
            $num = count($topic_list) - 1;
            $dateline = $topic_list[$num]['addtime'];
            $data['dateline'] = $dateline;
            $data['topic_list'] = $topic_list;
            json_result('加载成功', $data);
        } else {
            json_error("没有更多数据", $data);
        }
    }

    function Dig() {
        $tid = jget('tid');
        $uid = jget('uid');
        $mid = jget('mid');
        if ($tid > 0 && $uid > 0) {
            $count = DB::result_first("SELECT COUNT(*) FROM " . DB::table('topic_dig') . " WHERE tid='{$tid}' AND uid = '" . $mid . "'");
            if ($count > 0) {
                json_error("您已经赞过了");
            } else {
                $topic_info = DB::fetch_first("SELECT `uid`,`content`,`item_id`,`digcounts` FROM " . DB::table('topic') . " WHERE tid='{$tid}'");
                $uid = $topic_info['uid'];
                if ($uid == $mid) {
                    json_error("自己不能赞自己的");
                } else {
                    jtable('topic_more')->update_diguids($tid);
                    DB::query("update `" . DB::table('members') . "` set `digcount` = digcount + 1,`dig_new` = dig_new + 1 where `uid`='{$uid}'");
                    $ary = array('tid' => $tid, 'uid' => $mid, 'touid' => $uid, 'dateline' => time());
                    DB::insert('topic_dig', $ary, true);
                    jtable('topic')->update_digcounts($tid);
                    if (jconf::get('contest_available')) {
                        if ('contest' == DB::result_first("SELECT `item` FROM " . DB::table('topic') . " WHERE tid={$tid}")) {
                            jlogic('contest_entries')->update_dig($tid);
                        }
                    }
                    $credits = jconf::get('credits');
                    update_credits_by_action('topic_dig', $mid);
                    //update_credits_by_action('my_dig', $uid);
                    if ($GLOBALS['_J']['config']['feed_type'] && is_array($GLOBALS['_J']['config']['feed_type']) && in_array('dig', $GLOBALS['_J']['config']['feed_type']) && $GLOBALS['_J']['config']['feed_user'] && is_array($GLOBALS['_J']['config']['feed_user']) && array_key_exists($mid, $GLOBALS['_J']['config']['feed_user'])) {
                        $feed_msg = cut_str($topic_info['content'], 30, '');
                        feed_msg('leader', 'dig', $tid, $feed_msg, $topic_info['item_id']);
                    }
                    $data['digcounts'] = $topic_info['digcounts'] + 1;
                    json_result('谢谢您的支持', $data);
                }
            }
        }
    }

    function Do_forward() {
        $uid = jget('uid');
        if ($uid < 1) {
            json_error("请登录");
        }

        $content = strip_tags(jget('content'));

        $totid = jget('tid');
        $imageid = trim(jget('imageid'));
        $attachid = trim(jget('attachid'));

        $type = jget('topictype');
        $from = 'web';

        $is_reward = jget('is_reward');

        $item = trim(jget('item'));
        $item_id = intval(trim(jget('item_id')));
        $count = DB::result_first("SELECT COUNT(*) FROM " . DB::table('topic') . " WHERE totid='{$totid}' AND uid = '" . $uid . "'");
        if ($count > 0) {
            json_error("您已经转发过了");
        } else {
            #为有奖转发添加小尾巴
            if ($is_reward) {
                $reward = jlogic("reward")->getRewardInfo($is_reward);
                foreach ($reward["rules"]["user"] as $value) {
                    $uid = DB::result_first("select uid from `" . TABLE_PREFIX . "members` where nickname = '$value[nickname]' ");
                    !$uid || buddy_add($uid);
                }
            }
            if (!empty($item_id)) {
                jfunc('app');
                $ret = app_check($item, $item_id);
                if (!$ret) {
                    $item = '';
                    $item_id = 0;
                } else {
                    $from = $item;
                }
            } else {
                $item = '';
                $item_id = 0;
            }

            $data = array(
                'content' => $content,
                'totid' => $totid,
                'imageid' => $imageid,
                'attachid' => $attachid,
                'from' => $from,
                'type' => $type,
                'item' => $item,
                'item_id' => $item_id,
                #有奖转发标记
                'is_reward' => $is_reward,
                'uid' => $uid
            );

            $return = $this->TopicLogic->Add($data);

            if (is_array($return) && $return['tid'] > 0) {
                $topic_info = DB::fetch_first("SELECT `uid`,`content`,`item_id`,`forwards` FROM " . DB::table('topic') . " WHERE tid='{$totid}'");
                $json['forwards'] = $topic_info['forwards'];
                update_credits_by_action('share-one', $uid);
                $starttime = strtotime(date('Y-m-d 00:00:00', time()));
                $endtime = strtotime(date('Y-m-d 23:59:59', time()));
                $where_arr = array(
                    'uid' => $uid,
                    'type' => 'forward',
                    '>=@dateline' => $starttime,
                    '<=@dateline' => $endtime
                );
                $count = jtable('topic')->count($where_arr);
                if ($count == 2) {
                    update_credits_by_action('share-two', $uid);
                }
                if ($count == 3) {
                    update_credits_by_action('share-thire', $uid);
                }
                json_result('转发成功', $json);
            } else {
                $return = (is_string($return) ? "[转发失败]" . $return : (is_array($return) ? "[转发成功]但" . implode("", $return) : "未知错误"));
                json_error($return);
            }
        }
    }

    /**
     * 获取我的礼物
     */
    function SendGifts() {
        $mid = jget('mid', 'int');
        if ($mid < 1) {
            json_error("用户不存在");
        }
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("用户不存在");
        }
        $gid = jget('gid', 'int');
        if ($gid < 1) {
            json_error("礼物不存在");
        }
        $rs = jtable('my_gifts')->info(array('uid' => $mid, 'gid' => $gid));
        if (is_array($rs)) {
            if ($rs['num'] > 0) {
                $num = $rs['num'] - 1;
                jtable('my_gifts')->update(array('num' => $num), $rs['id']);
                $date = date('Y-m-d', time());
                /* $rank = jtable('ranking_gifts')->info(array('from_uid'=>$mid,'to_uid'=>$uid,'date'=>$date));
                  if(is_array($rank)){
                  $rank_num = $rank['num'] + 1;
                  jtable('ranking_gifts')->update(array('num'=>$rank_num),$rank['id']);
                  }else{
                  $savedata = array(
                  'from_uid' => $mid,
                  'to_uid' => $uid,
                  'num' => 1,
                  'date' => $date
                  );
                  jtable('ranking_gifts')->insert($savedata);
                  } */
                $m_info = jtable('members')->info(array('uid' => $mid));
                $u_info = jtable('members')->info(array('uid' => $uid));
                $savedata = array(
                    'from_uid' => $mid,
                    'from_nickname' => $m_info['nickname'],
                    'to_uid' => $uid,
                    'to_nickname' => $u_info['nickname'],
                    'gid' => $gid,
                    'num' => 1,
                    'date' => $date,
                    'datetime' => date('Y-m-d H:i:s', time())
                );
                jtable('ranking_gifts')->insert($savedata);
                update_credits_by_action('virtual_gifts', $mid);
                $n_time = date('Y-m-d', time());
                $RankGiftsLogic = jlogic('rank_gifts');
                $m_today_rs = $RankGiftsLogic->get_rankgifts_bygroup(" and from_uid = '$uid' and date = '$n_time'", ' group by from_uid');
                $m_today_total = 0;
                if ($m_today_rs) {
                    $m_today_total = $m_today_rs['rank_num'];
                }
                if ($m_today_total >= 10) {
                    update_credits_by_action('virtual_gifts_ten', $uid);
                }
                if (is_array($u_info) && is_array($m_info)) {
                    $info = jlogic("mall")->get_info($gid);
                    load::logic('pm');
                    $PmLogic = new PmLogic();
                    $post['to_user'] = $u_info['nickname'];
                    $post['title'] = "礼物动态";
                    $post['message'] = '赠送了礼物' . $info['name'] . '一个';
                    $PmLogic->pmSend($post, $mid, $m_info['username'], $m_info['nickname']);
                }
            }
        }
        json_result('加载成功', $rs);
    }

    public function DoSign() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("用户不存在");
        }
        $info = jtable('members')->info(array('uid' => $uid));
        if (is_array($info)) {
            $n_time = date('Y-m-d', time());
            $last_sign_time = $info['last_sign_time'];
            if ($last_sign_time != $n_time) {
                $score = 0;
                $credits = 0;
                $rule = jtable('credits_rule')->info(array('action' => '_S'));
                if (is_array($rule)) {
                    $credits = $rule['extcredits2'];
                }
                $sign_times = 1;
                if ($last_sign_time == '0000-00-00') {
                    $score = 0;
                } else if (date('Y-m', strtotime($last_sign_time)) != date('Y-m', strtotime($n_time))) {
                    $score = 0;
                } else if ($last_sign_time == date("Y-m-d", strtotime("-1 day"))) {
                    $score = $info['sign_times'] + 1;
                    $sign_times = $info['sign_times'] + 1;
                    $credits = $info['sign_times'] + 1;
                }
                jtable('members')->update(array('sign_times' => $sign_times, 'last_sign_time' => $n_time), array('uid' => $uid));
                update_credits_by_action('_S', $uid, 1, $score);
                $data['score'] = $credits;
                $data['sign_times'] = $sign_times;
                json_result('签到成功', $data);
            } else {
                json_error("一天只能签到一次哦");
            }
        } else {
            json_error("用户不存在");
        }
    }

    public function RankGifts() {
        $RankGiftsLogic = jlogic('rank_gifts');
        $mid = jget('mid', 'int');
        if ($mid < 1) {
            json_error("用户不存在");
        }
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("用户不存在");
        }
        $type = jget('type');
        $page = jget('page', 'int');
        if ($page < 1) {
            $page = 1;
        }
        $n_time = date('Y-m-d', time());
        $face = '';
        $m_total = 0;
        $m_today_total = 0;
        $u_total = 0;
        $m_rs = $RankGiftsLogic->get_rankgifts_bygroup(" and to_uid='$mid'", ' group by to_uid');
        $m_today_rs = $RankGiftsLogic->get_rankgifts_bygroup(" and to_uid='$mid' and date = '$n_time'", ' group by to_uid');
        $u_rs = $RankGiftsLogic->get_rankgifts_bygroup(" and from_uid='$uid' and to_uid='$mid'", ' group by to_uid');
        $info = jsg_member_info($mid, 'uid');
        if ($m_rs) {
            $m_total = $m_rs['rank_num'];
        }
        if (is_array($info)) {
            if (!stristr($info['face'], 'http://') && !stristr($info['face'], 'https://')) {
                $face = $GLOBALS['_J']['site_url'] . $info['face'];
            } else {
                $face = $info['face'];
            }
        }
        if ($m_today_rs) {
            $m_today_total = $m_today_rs['rank_num'];
        }
        if ($u_rs) {
            $u_total = $u_rs['rank_num'];
        }
        if ($type == 'today') {
            $where = " and to_uid='$mid' and date = '$n_time'";
        } else {
            $where = " and to_uid='$mid'";
        }
        $lists = $RankGiftsLogic->get_rankgiftslist_bygroup($where, $page);
        $ranklists = $RankGiftsLogic->get_rankgiftslist($where);
        $my_ranking = '暂未上榜';
        if (is_array($ranklists)) {
            foreach ($ranklists as $key => $value) {
                if ($uid == $value['from_uid']) {
                    $my_ranking = $key + 1;
                }
            }
        }
        $data['my_ranking'] = $my_ranking;
        $data['lists'] = $lists;
        $data['m_total'] = $m_total;
        $data['m_today_total'] = $m_today_total;
        $data['u_total'] = $u_total;
        $data['face'] = $face;
        $data['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('加载成功', $data);
    }

    public function MyGifts() {
        $MallLogic = jlogic('mall');
        $uid = jget('uid', 'int');
        $page = jget('page', 'int');
        if ($page < 1) {
            $page = 1;
        }
        if ($uid < 1) {
            json_error("用户不存在");
        }
        $where = " and g.uid='$uid' and num > 0";
        $gifts = $MallLogic->get_mygifts($where, $page);
        $data['site_url'] = $GLOBALS['_J']['site_url'];
        $data['gifts'] = $gifts;
        json_result('加载成功', $data);
    }

    public function MySendGifts() {
        $RankGiftsLogic = jlogic('rank_gifts');
        $uid = jget('uid', 'int');
        $page = jget('page', 'int');
        if ($page < 1) {
            $page = 1;
        }
        if ($uid < 1) {
            json_error("用户不存在");
        }
        $where = " and from_uid='$uid'";
        $gifts = $RankGiftsLogic->get_mysendgifts($where, $page);
        $data['site_url'] = $GLOBALS['_J']['site_url'];
        $data['gifts'] = $gifts;
        json_result('加载成功', $data);
    }

    public function GetRedEnvelopeTotal() {
        $datetime = date('Y-m-d H:i:s', time());
        $uid = 1;
        $sql_where = " and uid = '$uid' and status = '1' and stime <= '$datetime' and etime >= '$datetime' ";
        $rs = jlogic("activity")->get_red_envelopelog_tatal($sql_where);
        $undraw_amount = 0;
        if ($rs['undraw_amount']) {
            $undraw_amount = $rs['undraw_amount'];
        }
        $config = jconf::get('mall');
        $data['credits_name'] = $config['credits_name'];
        $data['undraw_amount'] = $undraw_amount;
        $data['uid'] = $uid;
        json_result('加载成功', $data);
    }

    public function GrabRedEnvelope() {
        $datetime = date('Y-m-d H:i:s', time());
        $sql_where = " status = '1' and stime <= '$datetime' and etime >= '$datetime' ";
        $list = jlogic("activity")->get_red_envelope_list($sql_where);
        $rs = array();
        if ($list['list']) {
            foreach ($list['list'] as $key => $value) {
                $rs[$key] = $value;
                $uid = $value['uid'];
                $member = jsg_member_info($uid);
                $rs[$key]['nickname'] = $member['nickname'];
                $rs[$key]['face_original'] = $member['face_original'];
                $rs[$key]['aboutme'] = $member['aboutme'];
            }
        }
        $data['lists'] = $rs;
        json_result('加载成功', $data);
    }

    public function QHB() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登录");
        }
        $gid = jget('gid', 'int');
        $sid = jget('mid', 'int');
        $datetime = date('Y-m-d H:i:s', time());
        if ($gid < 1) {
            $info = jtable('grab_red_envelope')->info(array('|@id' => $gid, 'uid' => $sid, 'status' => 1, '<=@stime' => $datetime, '>=@etime' => $datetime));
        } else {
            $info = jtable('grab_red_envelope')->info(array('id' => $gid, '|@uid' => $sid, 'status' => 1, '<=@stime' => $datetime, '>=@etime' => $datetime));
        }
        if (is_array($info)) {
            $info_log = jtable('grab_red_envelope_log')->info(array('pid' => $info['id'], 'grab_uid' => $uid));
            if (is_array($info_log)) {
                $data['gid'] = $info['id'];
                json_result("你已经领过了", $data);
            } else {
                if ($info['undraw_amount'] != 0) {
                    $uninfo_log = jtable('grab_red_envelope_log')->info(array('pid' => $info['id'], 'grab_status' => '0'));
                    if (is_array($uninfo_log)) {
                        $config = jconf::get('mall');
                        $data['credits_name'] = $config['credits_name'];
                        $data['grab_amount'] = $uninfo_log['grab_amount'];
                        jtable('grab_red_envelope_log')->update(array('grab_uid' => $uid, 'grab_status' => '1', 'grab_time' => $datetime), array('id' => $uninfo_log['id']));
                        $member = jsg_member_info($uid);
                        jtable('members')->update(array('extcredits2' => ($member['extcredits2'] + $uninfo_log['grab_amount'])), array('uid' => $uid));
                        jtable('grab_red_envelope')->update(array('draw_amount' => ($info['draw_amount'] + $uninfo_log['grab_amount']), 'undraw_amount' => ($info['undraw_amount'] - $uninfo_log['grab_amount']), 'grab_num' => ($info['grab_num'] + 1)), array('id' => $info['id']));
                        update_credits_by_action('grab_red_envelope', $uid);

                        $s_info = jsg_member_info($info['uid']);
                        load::logic('pm');
                        $PmLogic = new PmLogic();
                        $post['to_user'] = $member['nickname'];
                        $post['title'] = "红包动态";
                        $post['message'] = '恭喜您抢到' . $s_info['nickname'] . '的' . $uninfo_log['grab_amount'] . $config['credits_name'] . '的红包';
                        $PmLogic->pmSend($post, $info['uid'], $s_info['username'], $s_info['nickname']);
                        $data['gid'] = $info['id'];
                        json_result("恭喜你", $data);
                    } else {
                        $data['gid'] = $info['id'];
                        json_result("已经被抢完了", $data);
                    }
                } else {
                    $data['gid'] = $info['id'];
                    json_result("已经被抢完了", $data);
                }
            }
        } else {
            json_error("已经被抢完了");
        }
    }

    public function GrabRedEnvelopeLog() {
        $gid = jget('gid', 'int');
        if ($gid < 1) {
            json_error("这个排名不存在");
        }
        $page = jget('page', 'int');
        if ($page < 1) {
            $page = 1;
        }
        $sql_where = " pid='$gid' and grab_status = '1' ";
        $list = jlogic("activity")->get_red_envelopelog_list($sql_where, 'grab_amount desc');
        $rs = array();
        if ($list['list']) {
            foreach ($list['list'] as $key => $value) {
                $rs[$key] = $value;
                $uid = $value['grab_uid'];
                $member = jsg_member_info($uid);
                $rs[$key]['nickname'] = $member['nickname'];
                $rs[$key]['face_original'] = $member['face_original'];
            }
        }
        if ($list['page']) {
            if ($list['page']['total_page'] < $page) {
                $rs = array();
            }
        }
        $data['lists'] = $rs;
        json_result('加载成功', $data);
    }

    public function GetPmList() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $sql_where = " msgfromid = '$uid' or msgtoid = '$uid'";
        $list = jlogic("pm")->get_pm_list($sql_where);
        foreach ($list['list'] as $k => $one) {
            $list['list'][$k]['dateline'] = my_date_format($one['dateline']);
        }
        $data['lists'] = isset($list['list']) ? $list['list'] : array();
        json_result('加载成功', $data);
    }

    public function Interlocution() {
        $ctime = date('Y-m-d', time());
//        $info = jtable("interlocution")->info(array('answer_time'=>$ctime,'|@answer_time'=>'0000-00-00'));
        $sql = "select * FROM `" . DB::table('interlocution') . "` where answer_time ='{$ctime}' or answer_time = '0000-00-00' order by answer_time desc limit 1";
        $info = DB::fetch_first($sql);
        $data['info'] = $info;
        json_result('加载成功', $data);
    }

    public function DoInterlocution() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $id = jget('id', 'int');
        if ($id < 1) {
            json_error("问答题不存在");
        }
        $answer = jget('answer');
        $score = 0;
        $gold = 0;
        $is_right = 0;
        $info = jtable("interlocution")->info(array('id' => $id));
        if (is_array($info)) {
            $loginfo = jtable("interlocution_log")->info(array('pid' => $id, 'uid' => $uid));
            if (is_array($loginfo)) {
                json_error("只能答一次哦");
            } else {
                if ($answer == $info['answer']) {
                    $score = 100;
                    $is_right = 1;
                    $credits_rule = jconf::get('credits_rule');
                    if ($credits_rule['QA']) {
                        $gold = $credits_rule['QA']['extcredits2'];
                    }
                    update_credits_by_action('QA', $uid);
                }
                $member = jsg_member_info($uid);
                $ctime = date('Y-m-d', time());
                $savedata = array(
                    'uid' => $uid,
                    'username' => $member['nickname'],
                    'title' => $info['title'],
                    'answer' => $answer,
                    'is_right' => $is_right,
                    'score' => $score,
                    'gold' => $gold,
                    'pid' => $id,
                    'addtime' => $ctime
                );
                jtable("interlocution_log")->insert($savedata, 1);
                jtable("interlocution")->update(array('answer_time' => $ctime), array('id' => $id));
                $data['score'] = $score;
                $data['gold'] = $gold;
                json_result('加载成功', $data);
            }
        } else {
            json_error("问答题不存在");
        }
    }

    function WebInfo() {
        $web_info = jconf::get('web_info');
        json_result('加载成功', $web_info);
    }

    function Reply() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }

        $content = trim(jget('content'));

        if (!$content) {
            json_error("请输入内容");
        }

        $type = 'reply';

        $totid = jget('totid');
        $touid = jget('touid', 'int');
        $imageid = 0;
        $attachid = 0;
        $relateid = 0;
        $featureid = 0;
        $videoid = 0;
        $anonymous = 0;

        $longtextid = 0;
        $design = 0;
        $xiami_id = 0;
        $from = 'api';
        $verify = 0;

        $is_reward = jget('is_reward', 'int');


        $item = '';
        $item_id = 0;
        $data = array(
            'content' => $content,
            'uid' => $uid,
            'totid' => $totid,
            'imageid' => $imageid,
            'attachid' => $attachid,
            'videoid' => $videoid,
            'from' => empty($from) ? 'web' : $from,
            'type' => $type,
            'design' => $design,
            'item' => $item,
            'item_id' => $item_id,
            'touid' => $touid,
            'longtextid' => $longtextid,
            'xiami_id' => $xiami_id,
            'pverify' => $verify,
            'is_reward' => $is_reward,
            'relateid' => $relateid,
            'featureid' => $featureid,
            'anonymous' => $anonymous,
        );

        $return = $this->TopicLogic->Add($data);

        if (is_array($return) && $return['tid'] > 0) {
            json_result('评论成功', $return);
        } else {
            if (is_string($return)) {
                json_error("评论失败");
            } elseif (is_array($return)) {
                json_result('评论成功', $return);
            } else {
                json_error("未知错误");
            }
        }
    }

    public function AddBarrage() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $tip = jget('tip', 'int');
        $content = trim(jget('content'));
        if ($content == '') {
            json_error("请输入内容");
        }
        $savedata = array(
            'uid' => $uid,
            'tip' => $tip,
            'content' => $content,
            'addtime' => date('Y-m-d H:i:s', time())
        );
        $insertid = jtable('barrage')->insert($savedata, 1);
        $BarrageLogic = jlogic('barrage');
        $where = " and id = {$insertid}";
        $page = 1;
        $pagesize = 2;
        $order_by = 'b.id desc';
        $lists = $BarrageLogic->get_list_bypage($where, $page, $pagesize, $order_by);
        $list = array();
        if ($lists) {
            foreach ($lists as $key => $val) {
                $list[$key] = $this->TopicLogic->Make($val, 1);
            }
        }
        $data['list'] = $list;
        json_result('发表成功', $data);
    }

    public function ListBarrage() {
        $tip = jget('tip', 'int');
        $where = '';
        $BarrageLogic = jlogic('barrage');
        $page = jget('page');
        if (!isset($page)) {
            $page = 1;
        }
        $nextpage = $page;
        if ($tip > 0) {
            $where .= " and tip = {$tip}";
        }
        $pagesize = 2;
        $order_by = 'b.id desc';
        $lists = $BarrageLogic->get_list_bypage($where, $page, $pagesize, $order_by);
        $list = array();
        if (is_array($lists)) {
            foreach ($lists as $key => $val) {
                $list[$key] = $this->TopicLogic->Make($val, 1);
            }
            $nextpage = $page + 1;
        }
        $data['lists'] = $list;
        $data['nextpage'] = $nextpage;
        json_result('加载成功', $data);
    }

    public function DailyTasks() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $starttime = strtotime(date('Y-m-d 00:00:00', time()));
        $endtime = strtotime(date('Y-m-d 23:59:59', time()));
        $where = '';
        $where .=" and cycletype = '1' ";
        //and dateline >='$starttime' and dateline<='$endtime'
        $CreditsLogic = jlogic('credits');
        $order_by = '_order asc';
        $lists = $CreditsLogic->getdailytasks($where, $order_by);
        if (is_array($lists)) {
            foreach ($lists as $key => $val) {
                $where_arr = array(
                    'uid' => $uid,
                    'rid' => $val['rid'],
                    '>=@dateline' => $starttime,
                    '<=@dateline' => $endtime
                );
                $info = jtable('credits_log')->info($where_arr);
                $lists[$key]['isdo'] = '0';
                if (is_array($info)) {
                    $lists[$key]['isdo'] = '1';
                }
            }
        }
        $data['lists'] = $lists;
        json_result('加载成功', $data);
    }

    public function Audition() {
        $where = '';
        $AuditionLogic = jlogic('audition');
        $page = jget('page');
        if (!isset($page)) {
            $page = 1;
        }
        $nextpage = $page;
        $pagesize = 2;
        $order_by = 'id desc';
        $lists = $AuditionLogic->get_list_bypage($where, $page, $pagesize, $order_by);
        if (is_array($lists)) {
            $nextpage = $page + 1;
        }
        $data['lists'] = $lists;
        $data['nextpage'] = $nextpage;
        $data['site_url'] = $GLOBALS['_J']['site_url'];
        json_result('加载成功', $data);
    }

    public function AuditionDetail() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $id = jget('id');
        $info = jtable('audition')->info(array('id' => $id));
        if (!is_array($info)) {
            json_error("此歌曲不存在");
        }
        if (!stristr($info['file'], 'http://') && !stristr($info['file'], 'https://')) {
            $info['file'] = $GLOBALS['_J']['site_url'] . '/' . $info['file'];
        }
        if (!stristr($info['pic_file'], 'http://') && !stristr($info['pic_file'], 'https://')) {
            $info['pic_file'] = $GLOBALS['_J']['site_url'] . '/' . $info['pic_file'];
        }
        $data['info'] = $info;
        $data['site_url'] = '';
        json_result('加载成功', $data);
    }

    public function DoAudition() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $id = jget('id', 'int');
        if ($id < 1) {
            json_error("此歌曲不存在");
        }
        $info = jtable("audition")->info(array('id' => $id));
        if (is_array($info)) {
            $loginfo = jtable("audition_log")->info(array('pid' => $id, 'uid' => $uid));
            if (is_array($loginfo)) {
                json_error("只能试听一次哦");
            } else {
                update_credits_by_action('audition', $uid);
                $member = jsg_member_info($uid);
                $ctime = date('Y-m-d', time());
                $savedata = array(
                    'uid' => $uid,
                    'username' => $member['nickname'],
                    'title' => $info['title'],
                    'author' => $info['author'],
                    'pid' => $id,
                    'addtime' => $ctime
                );
                jtable("audition_log")->insert($savedata, 1);
                json_result('加载成功');
            }
        } else {
            json_error("问歌曲不存在");
        }
    }

    public function Look() {
        $where = '';
        $LookLogic = jlogic('look');
        $page = jget('page');
        if (!isset($page)) {
            $page = 1;
        }
        $nextpage = $page;
        $pagesize = 2;
        $order_by = 'id desc';
        $lists = $LookLogic->get_list_bypage($where, $page, $pagesize, $order_by);
        if (is_array($lists)) {
            foreach ($lists as $key => $val) {
                if (!stristr($val['file'], 'http://') && !stristr($val['file'], 'https://')) {
                    $lists[$key]['file'] = $GLOBALS['_J']['site_url'] . '/' . $val['file'];
                }
                if (!stristr($val['pic_file'], 'http://') && !stristr($val['pic_file'], 'https://')) {
                    $lists[$key]['pic_file'] = $GLOBALS['_J']['site_url'] . '/' . $val['pic_file'];
                }
            }
            $nextpage = $page + 1;
        }
        $data['lists'] = $lists;
        $data['nextpage'] = $nextpage;
        $data['site_url'] = '';
        json_result('加载成功', $data);
    }

    public function LookDetail() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $id = jget('id');
        $info = jtable('look')->info(array('id' => $id));
        if (!is_array($info)) {
            json_error("此视频不存在");
        }
        if (!stristr($info['file'], 'http://') && !stristr($info['file'], 'https://')) {
            $info['file'] = $GLOBALS['_J']['site_url'] . '/' . $info['file'];
        }
        if (!stristr($info['pic_file'], 'http://') && !stristr($info['pic_file'], 'https://')) {
            $info['pic_file'] = $GLOBALS['_J']['site_url'] . '/' . $info['pic_file'];
        }
        $data['info'] = $info;
        $data['site_url'] = '';
        json_result('加载成功', $data);
    }

    public function DoLook() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("请先登陆");
        }
        $id = jget('id', 'int');
        if ($id < 1) {
            json_error("此视频不存在");
        }
        $info = jtable("look")->info(array('id' => $id));
        if (is_array($info)) {
            $loginfo = jtable("look_log")->info(array('pid' => $id, 'uid' => $uid));
            if (is_array($loginfo)) {
                json_error("只能试看一次哦");
            } else {
                update_credits_by_action('look', $uid);
                $member = jsg_member_info($uid);
                $ctime = date('Y-m-d', time());
                $savedata = array(
                    'uid' => $uid,
                    'username' => $member['nickname'],
                    'title' => $info['title'],
                    'author' => $info['author'],
                    'pid' => $id,
                    'addtime' => $ctime
                );
                jtable("look_log")->insert($savedata, 1);
                json_result('加载成功');
            }
        } else {
            json_error("此视频不存在");
        }
    }

    public function SinaAuthCallback() {
        $bind_info_str = jget('info');
        $bind_info_str = stripslashes($bind_info_str);
        $bind_info = json_decode($bind_info_str, TRUE);
        if (is_array($bind_info)) {
            $sina_uid = $bind_info['id'];
            $xwb_bind_info = jtable('xwb_bind_info')->info(array('sina_uid' => $sina_uid));
            if (is_array($xwb_bind_info)) {
                $uid = $xwb_bind_info['uid'];
                $this->xwb_bind($uid, $bind_info);
                $this->login_info($uid);
            } else {
                $username = $nickname = jget('screen_name');
                $password = 'superstar';
                $timestamp = time();
                $sql_datas = array();
                $sql_datas['salt'] = jsg_member_salt();
                $sql_datas['password'] = jsg_member_password($password, $sql_datas['salt']);
                $sql_datas['nickname'] = $nickname;
                $sql_datas['username'] = ($username ? $username : '');
                $sql_datas['signature'] = $bind_info['description'];
                $sql_datas['role_type'] = 'normal';
                $sql_datas['role_id'] = (int) ($GLOBALS['_J']['config']['reg_email_verify'] ? $GLOBALS['_J']['config']['no_verify_email_role_id'] : $GLOBALS['_J']['config']['normal_default_role_id']);
                $sql_datas['invitecode'] = substr(md5(random(64)), -16);
                $sql_datas['regdate'] = $sql_datas['lastactivity'] = $timestamp;
                $uid = jtable('members')->insert($sql_datas, 1);
                $image_path = RELATIVE_ROOT_PATH . 'images/' . ($_J['config']['face_verify'] ? 'face_verify' : 'face') . '/' . face_path($uid);
                $image_name = $uid . '_b.jpg';
                $image_file = $image_path . $image_name;
                if (!is_dir($image_path)) {
                    jmkdir($image_path);
                }
                $IMG = file_get_contents($bind_info['avatar_large']);
                file_put_contents($image_file, $IMG);
                jlogic('image')->face_upload($image_file, $uid);
                $sys_config = jconf::get();
                if ($sys_config['ftp_on']) {
                    $ftp_key = randgetftp();
                    $get_ftps = jconf::get('ftp');
                    $site_url = $get_ftps[$ftp_key]['attachurl'];
                    $image_file = $attach_file = $site_url . '/' . str_replace('./', '', $image_file);
                }
                $sql = "update `" . TABLE_PREFIX . "members` set `face`='{$image_file}' where `uid`='$uid'";
                DB::query($sql);
                $this->xwb_bind($uid, $bind_info);
                $this->login_info($uid);
            }
        } else {
            json_error("获取用户信息失败");
        }
    }

    function xwb_bind($uid, $bind_info) {
        $ret = false;
        $uid = is_numeric($uid) ? $uid : 0;
        if ($uid > 0) {
            if (is_numeric($bind_info)) {
                $bind_info = array(
                    'uid' => (int) $bind_info
                );
            }
            $bind_info['uid'] = ($bind_info['uid'] ? $bind_info['uid'] : $bind_info['id']);
            jtable('xwb_bind_info')->delete(array('uid' => $uid));
            jtable('xwb_bind_info')->delete(array('sina_uid' => $bind_info['uid']));

            if ($bind_info['uid'] && $bind_info['access_token']) {
                $timestamp = time();
                $ret = DB::query("replace into " . TABLE_PREFIX . "xwb_bind_info
					(`uid`,`sina_uid`,`access_token`,`name`,`screen_name`,`domain`,`avatar_large`,`dateline`) values
				('$uid','{$bind_info['uid']}','{$bind_info['access_token']}','{$bind_info['name']}','{$bind_info['screen_name']}','{$bind_info['domain']}','{$bind_info['avatar_large']}','$timestamp')");
            }
        }

        return $ret;
    }

    public function QqAuthCallback() {
        $bind_info_str = jget('info');
        $bind_info_str = stripslashes($bind_info_str);
        $bind_info = json_decode($bind_info_str, TRUE);
        if (is_array($bind_info)) {
            $openid = $bind_info['openid'];
            $qq_bind_info = jtable('qq_bind_info')->info(array('openid' => $openid));
            if (is_array($qq_bind_info)) {
                $uid = $qq_bind_info['uid'];
                $this->qq_bind($uid, $bind_info);
                $this->login_info($uid);
            } else {
                $username = $nickname = $bind_info['nickname'];
                $password = 'superstar';
                $timestamp = time();
                $sql_datas = array();
                $sql_datas['salt'] = jsg_member_salt();
                $sql_datas['password'] = jsg_member_password($password, $sql_datas['salt']);
                $sql_datas['nickname'] = $nickname;
                $sql_datas['username'] = ($username ? $username : '');
                $sql_datas['role_type'] = 'normal';
                $sql_datas['role_id'] = (int) ($GLOBALS['_J']['config']['reg_email_verify'] ? $GLOBALS['_J']['config']['no_verify_email_role_id'] : $GLOBALS['_J']['config']['normal_default_role_id']);
                $sql_datas['invitecode'] = substr(md5(random(64)), -16);
                $sql_datas['regdate'] = $sql_datas['lastactivity'] = $timestamp;
                $uid = jtable('members')->insert($sql_datas, 1);

                $image_path = RELATIVE_ROOT_PATH . 'images/' . ($_J['config']['face_verify'] ? 'face_verify' : 'face') . '/' . face_path($uid);
                $image_name = $uid . '_b.jpg';
                $image_file = $image_path . $image_name;
                if (!is_dir($image_path)) {
                    jmkdir($image_path);
                }
                $IMG = file_get_contents($bind_info['figureurl_qq_2']);
                file_put_contents($image_file, $IMG);
                jlogic('image')->face_upload($image_file, $uid);
                $sys_config = jconf::get();
                if ($sys_config['ftp_on']) {
                    $ftp_key = randgetftp();
                    $get_ftps = jconf::get('ftp');
                    $site_url = $get_ftps[$ftp_key]['attachurl'];
                    $image_file = $attach_file = $site_url . '/' . str_replace('./', '', $image_file);
                }
                $sql = "update `" . TABLE_PREFIX . "members` set `face`='{$image_file}' where `uid`='$uid'";
                DB::query($sql);
                $this->qq_bind($uid, $bind_info);
                $this->login_info($uid);
            }
        } else {
            json_error("获取用户信息失败");
        }
    }

    function qq_bind($uid, $bind_info) {
        $uid = is_numeric($uid) ? $uid : 0;
        if ($uid > 0) {
            jtable('qq_bind_info')->delete(array('uid' => $uid));
            jtable('qq_bind_info')->delete(array('openid' => $bind_info['openid']));
            if ($bind_info['openid']) {
                $timestamp = time();
                $bind_info['uid'] = $uid;
                $bind_info['dateline'] = $timestamp;
                jtable('qq_bind_info')->insert($bind_info);
            }
        }
    }

    public function WxAuthCallback() {
        $bind_info_str = jget('info');
        $bind_info_str = stripslashes($bind_info_str);
        $bind_info = json_decode($bind_info_str, TRUE);
        if (is_array($bind_info)) {
            $openid = $bind_info['openid'];
            $wx_bind_info = jtable('wx_bind_info')->info(array('openid' => $openid));
            if (is_array($wx_bind_info)) {
                $uid = $wx_bind_info['uid'];
                $this->wx_bind($uid, $bind_info);
                $this->login_info($uid);
            } else {
                $username = $nickname = $bind_info['nickname'];
                $password = 'superstar';
                $timestamp = time();
                $sql_datas = array();
                $sql_datas['salt'] = jsg_member_salt();
                $sql_datas['password'] = jsg_member_password($password, $sql_datas['salt']);
                $sql_datas['nickname'] = $nickname;
                $sql_datas['username'] = ($username ? $username : '');
                $sql_datas['role_type'] = 'normal';
                $sql_datas['role_id'] = (int) ($GLOBALS['_J']['config']['reg_email_verify'] ? $GLOBALS['_J']['config']['no_verify_email_role_id'] : $GLOBALS['_J']['config']['normal_default_role_id']);
                $sql_datas['invitecode'] = substr(md5(random(64)), -16);
                $sql_datas['regdate'] = $sql_datas['lastactivity'] = $timestamp;
                $uid = jtable('members')->insert($sql_datas, 1);

                $image_path = RELATIVE_ROOT_PATH . 'images/' . ($_J['config']['face_verify'] ? 'face_verify' : 'face') . '/' . face_path($uid);
                $image_name = $uid . '_b.jpg';
                $image_file = $image_path . $image_name;
                if (!is_dir($image_path)) {
                    jmkdir($image_path);
                }
                $IMG = file_get_contents($bind_info['headimgurl']);
                file_put_contents($image_file, $IMG);
                jlogic('image')->face_upload($image_file, $uid);
                $sys_config = jconf::get();
                if ($sys_config['ftp_on']) {
                    $ftp_key = randgetftp();
                    $get_ftps = jconf::get('ftp');
                    $site_url = $get_ftps[$ftp_key]['attachurl'];
                    $image_file = $attach_file = $site_url . '/' . str_replace('./', '', $image_file);
                }
                $sql = "update `" . TABLE_PREFIX . "members` set `face`='{$image_file}' where `uid`='$uid'";
                DB::query($sql);
                $this->wx_bind($uid, $bind_info);
                $this->login_info($uid);
            }
        } else {
            json_error("获取用户信息失败");
        }
    }

    function wx_bind($uid, $bind_info) {
        $uid = is_numeric($uid) ? $uid : 0;
        if ($uid > 0) {
            jtable('wx_bind_info')->delete(array('uid' => $uid));
            jtable('wx_bind_info')->delete(array('openid' => $bind_info['openid']));
            if ($bind_info['openid']) {
                $timestamp = time();
                $bind_info['uid'] = $uid;
                $bind_info['dateline'] = $timestamp;
                jtable('wx_bind_info')->insert($bind_info);
            }
        }
    }

    public function GetReplyList() {
        $tid = jget('tid', 'int');
        $page = jget('page', 'int');
        $per_page_num = 10;
        if ($tid < 1) {
            json_error("此动态不存在");
        }
        if ($page < 1) {
            $page = 1;
        }
        $topic_info = $this->TopicLogic->Get($tid);

        if ($topic_info['type'] == 'reply') {
            $roottid = $topic_info['roottid'];
            $root_type = DB::result_first("SELECT `type` FROM " . DB::table('topic') . " WHERE tid='{$roottid}'");
        } else {
            $root_type = $topic_info['type'];
        }

        if (!$topic_info) {
            json_error("此动态不存在");
        }

        $reply_list = array();
        $total_page = 0;
        if ($topic_info['replys'] > 0) {
            $p = array(
                'perpage' => $per_page_num,
                'page_var' => 'page',
            );
            $orderby = jget('orderby');
            if ('dig' == $orderby) {
                $p['sql_order'] = ' `digcounts` DESC, `lastdigtime` DESC ';
            } elseif ('post' == $orderby) {
                $p['sql_order'] = ' `dateline` DESC ';
            } else {
                $p['sql_order'] = ' `dateline` ASC ';
            }
            $rets = jtable('topic_relation')->get_list($topic_info['tid'], $p);
            if ($rets) {
                $reply_list = $rets['list'];
                $total_page = $rets['page']['total_page'];
            }
        }
        $data['page_count'] = $total_page;
        $data['list'] = $reply_list;
        json_result('加载成功', $data);
    }

    public function GetDigList() {
        $tid = jget('tid', 'int');
        $page = jget('page', 'int');
        $page_size = 10;
        if ($tid < 1) {
            json_error("此动态不存在");
        }
        if ($page < 1) {
            $page = 1;
        }
        $topic_info = $this->TopicLogic->Get($tid);

        $topic_info['type'] = 'dig';

        if (!$topic_info) {
            json_error("此动态不存在");
        }

        $dig_users = array();
        if ($topic_info['digcounts'] > 0) {
            $offset = ($page - 1) * $page_size;
            $query = DB::query("SELECT uid FROM `" . TABLE_PREFIX . "topic_dig` WHERE tid = '" . $topic_info['tid'] . "' ORDER BY id DESC  LIMIT {$offset} , {$page_size}");
            while ($rs = DB::fetch($query)) {
                $user[$rs['uid']] = $rs['uid'];
            }
            $dig_users = $this->TopicLogic->GetMember($user, "`uid`,`username`,`nickname`,`face`");
            foreach ($dig_users as $k => $v) {
                array_splice($dig_users[$k], 4, 3);
            }
        }
        json_result('加载成功', $dig_users);
    }

    public function GetActivityList() {
        $page = jget('page', 'int');
        $search_key = jget('search_key', 'txt');
        $dateline = jget('dateline', 'txt');
        $pagesize = 10;
        if ($page < 1) {
            $page = 1;
        }
        $where = " and topic_type = 1 ";
        $time = date('Y-m-d H:i:s', time());
        if ($dateline == 'before') {
            $where .= " and e_time < '$time'";
        } else if ($dateline == 'after') {
            $where .= " and e_time >= '$time'";
        }
        if ($search_key != '') {
            $where .= " and title like '%$search_key%'";
        }
        $order_by = 'tid desc';
        $lists = $this->TopicLogic->get_lists($where, $page, $pagesize, $order_by);
        $list = array();
        if ($lists) {
            foreach ($lists as $key => $val) {
                $list[$key] = $this->TopicLogic->Make($val, 1);
            }
        }
        $data['lists'] = $list;
        json_result('加载成功', $data);
    }

    public function GetArticleList() {
        $page = jget('page', 'int');
        $uid = jget('uid', 'int');
        $search_key = jget('search_key', 'txt');
        $pagesize = 10;
        if ($uid < 1) {
            json_error("用户不存在");
        }
        if ($page < 1) {
            $page = 1;
        }
        $where = " and uid = {$uid} and topic_type = 2 ";
        if ($search_key != '') {
            $where .= " and title like '%$search_key%'";
        }
        $order_by = 'tid desc';
        $lists = $this->TopicLogic->get_lists($where, $page, $pagesize, $order_by);
        $list = array();
        if ($lists) {
            foreach ($lists as $key => $val) {
                $list[$key] = $this->TopicLogic->Make($val, 1);
            }
        }
        $data['lists'] = $list;

        json_result('加载成功', $data);
    }

    public function TopicReply() {
        $uid = jget('uid', 'int');
        if ($uid < 1) {
            json_error("用户不存在");
        }

        $content = trim(jget('content'));
        $title = '';
        $describe = '';
        if (!$content) {
            json_error("请输入内容");
        }

        $topic_type = 'reply';
        $t_type = 0;
        $s_time = '';
        $e_time = '';
        $poster = '';

        $type = 'reply';


        $roottid = jget('totid', 'int');
        $totid = jget('totid', 'int');
        $touid = 0;
        $imageid = 0;
        $attachid = 0;
        $relateid = 0;
        $featureid = 0;
        $videoid = 0;
        $anonymous = 0;

        $longtextid = 0;
        $design = 'vc';
        $xiami_id = 0;
        $from = '';
        $verify = 0;
        $is_reward = jget('is_reward', 'int');


        $item = '';
        $item_id = 0;
        $data = array(
            'title' => $title,
            'describe' => $describe,
            'content' => $content,
            's_time' => $s_time,
            'e_time' => $e_time,
            'poster' => $poster,
            'topic_type' => $t_type,
            'uid' => $uid,
            'totid' => $totid,
            'imageid' => $imageid,
            'attachid' => $attachid,
            'videoid' => $videoid,
            'from' => empty($from) ? 'web' : $from,
            'type' => $type,
            'design' => $design,
            'item' => $item,
            'item_id' => $item_id,
            'touid' => $touid,
            'longtextid' => $longtextid,
            'xiami_id' => $xiami_id,
            'pverify' => $verify,
            #有奖转发标记
            'is_reward' => $is_reward,
            'relateid' => $relateid,
            'featureid' => $featureid,
            'anonymous' => $anonymous,
        );


        $return = $this->TopicLogic->Add($data);

        if (is_array($return) && $return['tid'] > 0) {
            json_result('评论成功');
        } else {
            if (is_string($return)) {
                json_error("评论失败" . $return);
            } elseif (is_array($return)) {
                json_result('评论成功');
            } else {
                json_error("评论失败");
            }
        }
    }
    function MyFollows() {
        $uid = jget('uid', 'int');
        $page_num = 10;
        $page = jget('page');
        $p = array(
            'page_num' => $page_num,
            'uid' => $uid
        );
        $rets = jlogic('buddy_follow')->get($p);
        $data['list'] = $rets['list'];
        $data['total_page'] = isset($rets['page']['total_page'])?$rets['page']['total_page']:0;
        $data['current_page'] = isset($rets['page']['current_page'])?$rets['page']['current_page']:0;
        $data['page_next'] = $data['current_page'] + 1;
        json_result('加载成功',$data);
    }
    function CheckWx() {
        $is_show = 'false';
        $data['is_show'] = $is_show;
        json_result('加载成功',$data);
    }
}

?>