<?php

/**
 * 功能描述：支付回调
 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }

    function Execute() {

        switch ($this->Code) {
            case 'index':
                $this->Wxpay();
                break;
            default:
                $this->Wxpay();
                break;
        }
    }
    public function test(){
        echo "aaa";
    }
    public function Wxpay() {
        require_once ROOT_PATH."include/class/PayNotifyCallBack.class.php";
        $notify = new PayNotifyCallBack();
        $notify->Handle(false);
    }
}
?>