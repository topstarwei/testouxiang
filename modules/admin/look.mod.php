<?php

/**
 *
 * 试看管理模块
 *

 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }
    function Execute() {
        ob_start();
        switch ($this->Code) {
            case 'add': {
                $this->Add();
                break;
            }
            case 'doadd': {
                $this->DoAdd();
                break;
            }
            case 'modify': {
                $this->Modify();
                break;
            }
            case 'domodify': {
                $this->DoModify();
                break;
            }
            case 'del': {
                $this->Del();
                break;
            }
            case 'lists': {
                $this->Lists();
                break;
            }
            case 'log_list': {
                $this->LogList();
                break;
            }
            default : {
                $this->Main();
                break;
            }
        }
        $this->ShowBody(ob_get_clean());
    }

    function Main() {
        
    }
    public function Lists() {
        $list = jlogic("look")->get_list();
        include template('admin/look_list');
    }
    public function Add() {
        $title = "添加试看";
        $action = 'admin.php?mod=look&code=doadd';
        include template('admin/add_look');
    }
    public function DoAdd(){
        $title = jget('title');
        //$author = jget('author');
        //$special = jget('special');
        if($title == ''){
            $this->Messager("标题不能为空");
        }
//        if($author == ''){
//            $this->Messager("歌手不能为空");
//        }
        $field = "look";
        $attach_type = strtolower(end(explode('.', $_FILES[$field]['name'])));
        if ($attach_type != 'mp4') {
                $this->Messager("视频格式不对");
        }
        $image_src = '';
        if ($_FILES['image']) {
            $image = jlogic('image')->upload(array('pic_field' => 'image'));
            $image['photo'] = $image['site_url'] ? $image['site_url'] . '/' . str_replace('./', '', $image['photo']) : $image['photo'];
            $image_src = (string) $image['photo'];
        }
        $savedata = array(
            'title' => $title,
            //'author' => $author,
            //'special' => $special,
            'pic_file' => $image_src,
            'addtime' => date('Y-m-d H:i:s', time())
        );
        $attach_id = jtable("look")->insert($savedata, 1);
        
        $attach_path = RELATIVE_ROOT_PATH . 'data/attachs/' . $field . '/' . face_path($attach_id);
        
        $attach_name = $attach_id . '.' . $attach_type;
        $attach_file = $attach_path . $attach_name;
        if (!is_dir($attach_path)) {
            jio()->MakeDir($attach_path);
        }

        $attach_size = min((is_numeric($this->Config['attach_size_limit']) ? $this->Config['attach_size_limit'] : 1024), 51200);
        jupload()->init($attach_path, $field, false, true);
        jupload()->setMaxSize($attach_size);
        jupload()->setNewName($attach_name);
        $ret = jupload()->doUpload();
        $attach_size = filesize($attach_file);
        $sys_config = jconf::get();
        if ($sys_config['ftp_on']) {
            $ftp_key = randgetftp();
            $get_ftps = jconf::get('ftp');
            $site_url = $get_ftps[$ftp_key]['attachurl'];

            $ftp_result = ftpcmd('upload', $attach_file, '', $ftp_key);
            if ($ftp_result > 0) {

                jio()->DeleteFile($attach_file);

                $attach_file = $site_url . '/' . str_replace('./', '', $attach_file);
            }
        }
        $p = array(
            'file' => $attach_file,
            'filetype' => $attach_type,
            'filesize' => $attach_size,
        );
        jtable("look")->update($p, array('id'=>$attach_id));
        $this->Messager("添加成功");
    }
    public function Modify(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        $title = "试看编辑";
        $action = 'admin.php?mod=look&code=domodify';
        $info = jtable("look")->info(array('id'=>$id));
        
        if($info){
            $info['content'] = json_decode($info['content'], TRUE);
        }else{
            $info['content'] = '';
        }
        include template('admin/modify_look');
    }
    public function DoModify(){
        $attach_id = $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        $title = jget('title');
        //$author = jget('author');
        //$special = jget('special');
        if($title == ''){
            $this->Messager("标题不能为空");
        }
//        if($author == ''){
//            $this->Messager("歌手不能为空");
//        }
        
        
        
        $savedata = array(
            'title' => $title,
            //'author' => $author,
            //'special' => $special
        );
        
        if ($_FILES['image']['error'] === 0) {
            $image = jlogic('image')->upload(array('pic_field' => 'image'));
            $image['photo'] = $image['site_url'] ? $image['site_url'] . '/' . str_replace('./', '', $image['photo']) : $image['photo'];
            $image_src = (string) $image['photo'];
            $savedata['pic_file'] = $image_src;
        }
        $field = "look";
        if ($_FILES[$field]['error'] === 0) {
           
            $attach_type = strtolower(end(explode('.', $_FILES[$field]['name'])));
            if ($attach_type != 'mp4') {
                    $this->Messager("视频格式不对");
            }
            $attach_path = RELATIVE_ROOT_PATH . 'data/attachs/' . $field . '/' . face_path($attach_id);

            $attach_name = $attach_id . '.' . $attach_type;
            $attach_file = $attach_path . $attach_name;
            if (!is_dir($attach_path)) {
                jio()->MakeDir($attach_path);
            }

            $attach_size = min((is_numeric($this->Config['attach_size_limit']) ? $this->Config['attach_size_limit'] : 1024), 102400);
            jupload()->init($attach_path, $field, false, true);
            jupload()->setMaxSize($attach_size);
            jupload()->setNewName($attach_name);
            $ret = jupload()->doUpload();
            
            $attach_size = filesize($attach_file);
            $sys_config = jconf::get();
            if ($sys_config['ftp_on']) {
                $ftp_key = randgetftp();
                $get_ftps = jconf::get('ftp');
                $site_url = $get_ftps[$ftp_key]['attachurl'];

                $ftp_result = ftpcmd('upload', $attach_file, '', $ftp_key);
                if ($ftp_result > 0) {

                    jio()->DeleteFile($attach_file);

                    $attach_file = $site_url . '/' . str_replace('./', '', $attach_file);
                }
            }
            $savedata['file'] = $attach_file;
            $savedata['filetype'] = $attach_type;
            $savedata['filesize'] = $attach_size;
        }
        jtable("look")->update($savedata, array('id'=>$id));
        $this->Messager("修改成功");
    }
    public function Del(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        
        jtable("look")->delete(array('id'=>$id));
        $this->Messager("删除成功");
    }
    public function LogList() {
        $list = jlogic("look")->get_log_list();
        include template('admin/looklog_list');
    }
}

?>
