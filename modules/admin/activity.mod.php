<?php

/**
 *
 * 游戏管理模块
 *

 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    private $status = array(
        0 => "未开启",
        1 => "已开启",
    );
    private $grab_status = array(
        0 => "未领取",
        1 => "已领取",
    );
    private $act_type = array(
        0 => "普通红包",
        1 => "手气红包",
    );
    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }

    function Execute() {
        ob_start();
        switch ($this->Code) {
            case 'grab_red_envelope': {
                $this->GrabRedEnvelope();
                break;
            }
            case 'add_red_envelope': {
                $this->AddRedEnvelope();
                break;
            }
            case 'doadd_red_envelope': {
                $this->DoAddRedEnvelope();
                break;
            }
            case 'modily_red_envelope': {
                $this->ModilyRedEnvelope();
                break;
            }
            case 'grab_red_envelope_log': {
                $this->GrabRedEnvelopeLog();
                break;
            }
            default : {
                $this->Main();
                break;
            }
        }
        $this->ShowBody(ob_get_clean());
    }

    function Main() {
        
    }

    public function GrabRedEnvelope() {
        $list = jlogic("activity")->get_red_envelope_list();
        if($list['list']){
            foreach ($list['list'] as $key => $value) {
                $uid = $value['uid'];
                $member = jsg_member_info($uid);
                $list['list'][$key]['nickname'] = $member['nickname'];
                $list['list'][$key]['status_name'] = $this->status[$value['status']];
                $list['list'][$key]['act_type_name'] = $this->act_type[$value['act_type']];
            }
        }
        include template('admin/grab_red_envelope');
    }
    public function AddRedEnvelope() {
        $action = 'admin.php?mod=activity&code=doadd_red_envelope';
        include template('admin/add_red_envelope');
    }
    public function DoAddRedEnvelope() {
        $uid = jget("radio_uid",'int');
        if($uid < 1){
            $this->Messager("请选择用户");
        }
        $act_type = jget('act_type','int');
        $amount = jget('amount','int');
        $title = jget('title','txt');
        $num = jget('num','int');
        if($amount < $num){
            $this->Messager("红包个数不能大于红包总额");
        }
        if($act_type == 0){
            if($amount%$num != 0){
                $this->Messager("红包不能整除");
            }
        }
        $stime = jget('stime');
        $etime = jget('etime');
        $status = jget('status');
        $savedata = array(
            'uid' => $uid,
            'title' => $title,
            'amount' => $amount,
            'num' => $num,
            'stime' => $stime,
            'etime' => $etime,
            'status' => $status,
            'undraw_amount' => $amount,
            'ctime' => date('Y-m-d H:i:s', time())
        );
        $insertid = jtable("grab_red_envelope")->insert($savedata, 1);
        if($act_type == '0' || $amount == $num){
            $grab_amount = $amount/$num;
            for($i=1;$i <= $num;$i++){
                $logdata = array(
                    'pid' => $insertid,
                    'grab_amount' => $grab_amount
                );
                jtable("grab_red_envelope_log")->insert($logdata, 1);
            }
        }else{
            $arr = $this->get_rand($amount, $num);
            foreach ($arr as $value) {
                $logdata = array(
                    'pid' => $insertid,
                    'grab_amount' => $value['money']
                );
                jtable("grab_red_envelope_log")->insert($logdata, 1);
            }
        }
        $this->Messager("添加成功");
    }
    /**
     * 
     * @param type $total 红包总额  
     * @param int $num 分成$num个红包，支持$num人随机领取 
     */
    function get_rand($total,$num){
        $min=1;//每个人最少能收到1元  
        for ($i=1;$i<$num;$i++) {  
            $safe_total=ceil(($total-($num-$i)*$min)/($num-$i));//随机安全上限  
            $money=mt_rand($min,$safe_total);  
            $total=$total-$money; 
                $arr[$i] = array(
                        'i' => $i,
                        'money' => $money,
                        'total' => $total
                );
        } 
        $arr[$num] = array('i'=>$num,'money'=>$total,'total'=>0);
        return $arr;
    }
    public function ModilyRedEnvelope(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("请选择一条数据");
        }
        $status = jget('status','int');
        $data = array(
            'status' => $status
        );
        $r = jtable("grab_red_envelope")->update($data, $id);
        $msg = "关闭";
        if($status == 1){
            $msg = "开启";
        }
        return $r ? $this->Messager($msg."成功") : $this->Messager($msg."失败");
    }
    public function GrabRedEnvelopeLog() {
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("请选择一条数据");
        }
        $sql_where = " pid=".$id;
        $list = jlogic("activity")->get_red_envelopelog_list($sql_where);
        if($list['list']){
            foreach ($list['list'] as $key => $value) {
                $uid = $value['grab_uid'];
                $member = jsg_member_info($uid);
                $list['list'][$key]['nickname'] = $member['nickname'];
                $list['list'][$key]['grab_status_name'] = $this->grab_status[$value['grab_status']];
            }
        }
        include template('admin/grab_red_envelope_log');
    }
}

?>
