<?php

/**
 *
 * 问答管理模块
 *

 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }
    private $status = array(
        0 => "错误",
        1 => "正确"
    );
    function Execute() {
        ob_start();
        switch ($this->Code) {
            case 'add': {
                $this->Add();
                break;
            }
            case 'doadd': {
                $this->DoAdd();
                break;
            }
            case 'modify': {
                $this->Modify();
                break;
            }
            case 'domodify': {
                $this->DoModify();
                break;
            }
            case 'del': {
                $this->Del();
                break;
            }
            case 'lists': {
                $this->Lists();
                break;
            }
            case 'log_list': {
                $this->LogList();
                break;
            }
            default : {
                $this->Main();
                break;
            }
        }
        $this->ShowBody(ob_get_clean());
    }

    function Main() {
        
    }
    public function Lists() {
        $list = jlogic("interlocution")->get_list();
        include template('admin/interlocution_list');
    }
    public function Add() {
        $title = "添加问答";
        $action = 'admin.php?mod=interlocution&code=doadd';
        include template('admin/add_interlocution');
    }
    public function DoAdd(){
        $title = jget('title');
        $answer = jget('answer');
        if($title == ''){
            $this->Messager("标题不能为空");
        }
        if($answer == ''){
            $this->Messager("答案不能为空");
        }
        $identity = $_POST['identity'];
        $arr = array();
        foreach ($identity as $key => $val) {
            $arr[$key]['identity'] = urlencode($val);
            $arr[$key]['answer_txt'] = urlencode($_POST['answer_txt'][$key]);
        }
        $savedata = array(
            'title' => $title,
            'answer' => $answer,
            'content' => urldecode(json_encode($arr)),
            'addtime' => date('Y-m-d', time())
        );
        jtable("interlocution")->insert($savedata, 1);
        $this->Messager("添加成功");
    }
    public function Modify(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        $title = "问答编辑";
        $action = 'admin.php?mod=interlocution&code=domodify';
        $info = jtable("interlocution")->info(array('id'=>$id));
        
        if($info){
            $info['content'] = json_decode($info['content'], TRUE);
        }else{
            $info['content'] = '';
        }
        include template('admin/modify_interlocution');
    }
    public function DoModify(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        $title = jget('title');
        $answer = jget('answer');
        if($title == ''){
            $this->Messager("标题不能为空");
        }
        if($answer == ''){
            $this->Messager("答案不能为空");
        }
        $identity = $_POST['identity'];
        $arr = array();
        foreach ($identity as $key => $val) {
            $arr[$key]['identity'] = urlencode($val);
            $arr[$key]['answer_txt'] = urlencode($_POST['answer_txt'][$key]);
        }
        $savedata = array(
            'title' => $title,
            'answer' => $answer,
            'content' => urldecode(json_encode($arr))
        );
        jtable("interlocution")->update($savedata, array('id'=>$id));
        $this->Messager("修改成功");
    }
    public function Del(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        
        jtable("interlocution")->delete(array('id'=>$id));
        $this->Messager("删除成功");
    }
    public function LogList() {
        $list = jlogic("interlocution")->get_log_list();
        $status = $this->status;
        include template('admin/interlocutionlog_list');
    }
}

?>
