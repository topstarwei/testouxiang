<?php

/**
 *
 * 摩币充值管理模块
 *

 */
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }
    private $order_status = array(
        0 => "未支付",
        1 => "已支付",
        2 => "已发货",
        3 => "已退还",
    );
    private $pay_type = array(
        'wx' => "微信支付",
        'alipay' => "支付宝支付",
    );
    function Execute() {
        ob_start();
        switch ($this->Code) {
            case 'add': {
                $this->Add();
                break;
            }
            case 'doadd': {
                $this->DoAdd();
                break;
            }
            case 'modify': {
                $this->Modify();
                break;
            }
            case 'domodify': {
                $this->DoModify();
                break;
            }
            case 'del': {
                $this->Del();
                break;
            }
            case 'list': {
                $this->Lists();
                break;
            }
            case 'log_list': {
                $this->LogList();
                break;
            }
            default : {
                $this->Main();
                break;
            }
        }
        $this->ShowBody(ob_get_clean());
    }

    function Main() {
        
    }
    public function Lists() {
        $list = jlogic("recharge")->get_list();
        include template('admin/recharge_list');
    }
    public function Add() {
        $action = 'admin.php?mod=recharge&code=doadd';
        include template('admin/add_recharge');
    }
    public function DoAdd(){
        $amount = jget('amount','int');
        $get_coin = jget('get_coin','int');
        if($amount == 0){
            $this->Messager("金额必须大于0");
        }
        if($get_coin == 0){
            $this->Messager("获得摩币数量必须大于0");
        }
        
        $savedata = array(
            'amount' => $amount,
            'get_coin' => $get_coin,
            'addtime' => date('Y-m-d H:i:s', time())
        );
        jtable("recharge")->insert($savedata, 1);
        $this->Messager("添加成功");
    }
    public function Modify(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        $action = 'admin.php?mod=recharge&code=domodify';
        $info = jtable("recharge")->info(array('id'=>$id));
        include template('admin/modify_recharge');
    }
    public function DoModify(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        $amount = jget('amount');
        $get_coin = jget('get_coin');
        if($amount == 0){
            $this->Messager("金额必须大于0");
        }
        if($get_coin == 0){
            $this->Messager("获得摩币数量必须大于0");
        }
        
        $savedata = array(
            'amount' => $amount,
            'get_coin' => $get_coin,
        );
        jtable("recharge")->update($savedata, array('id'=>$id));
        $this->Messager("修改成功");
    }
    public function Del(){
        $id = jget('id','int');
        if($id < 1){
            $this->Messager("此ID不存在");
        }
        
        jtable("recharge")->delete(array('id'=>$id));
        $this->Messager("删除成功");
    }
    public function LogList() {
        $list = jlogic("recharge")->get_log_list();
        if($list['list']){
            foreach ($list['list'] as $key => $value) {
                $uid = $value['r_uid'];
                $member = jsg_member_info($uid);
                $list['list'][$key]['nickname'] = $member['nickname'];
                $list['list'][$key]['status_name'] = $this->order_status[$value['pay_status']];
                $list['list'][$key]['pay_type_name'] = $this->pay_type[$value['pay_type']];
            }
        }
        include template('admin/rechargelog_list');
    }
}

?>
