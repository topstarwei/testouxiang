<?php
if (!defined('IN_JISHIGOU')) {
    exit('invalid request');
}

class ModuleObject extends MasterObject {

    function ModuleObject($config) {
        $this->MasterObject($config);

        $this->Execute();
    }

    function Execute() {

        switch ($this->Code) {
            case 'index':
                $this->Index();
                break;
            case 'demo':
                $this->Demo();
                break;
            default:
                $this->Index();
                break;
        }
    }


    var $appKey = "wxdc3d848c1751519f";
    var $appSecret = "e0445db600558e08d838b7cd3ab03ea3";

    public function Index() {
        $id = jget('id','int');
        if($id < 1){
            echo "内容不存在";
            exit;
        }
        $signPackage = $this->shareapi();
        //先获取文章信息
        $topic_info = jlogic('longtext')->get_info($id, array('type' => 'artZoom2'));
        include template('share/showsharepage');
    }

    //分享参数
    public function shareapi() {
        require_once ROOT_PATH . 'include/class/jssdk.class.php';
        $jssdk = new JSSDK("ouxiang", $this->appKey, $this->appSecret);
        $signPackage = $jssdk->GetSignPackage();
        return $signPackage;
    }
    public function demo(){
        $id = jget('id','int');
        if($id < 1){
            echo "内容不存在";
            exit;
        }
        //先获取文章信息
        $topic_info = jlogic('longtext')->get_info($id, array('type' => 'artZoom2'));
        var_dump($topic_info);
    }
}
